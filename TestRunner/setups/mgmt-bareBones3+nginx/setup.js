const fs = require( 'fs' );
const path = require( 'path' );

module.exports = {
    up: function( proxmox, vars ) {
        function createNode( index ) {
            return new Promise( async ( resolve ) => {
                const runner = require( '../../src/runner' );

                var vm = await proxmox.cloneVM( 207, 'BAREBONES' + index, true );

                await proxmox.startVM( vm );

                runner.awaitNode( await proxmox.getMACAddress( vm ), async ( node ) => {
                    await node.transmitFolder( path.join( __dirname, '../../projects/bernrodeWeb' ), '/opt/bernrodeWeb' );
                    await node.transmitFile  ( path.join( __dirname, './install.sh' )              , '/tmp/installBernrodeWeb.sh' );
                    await node.executeCommand( '/bin/bash', [ '/tmp/installBernrodeWeb.sh' ]       , '/opt/bernrodeWeb' );
                    
                    node.executeCommand( 'node', [ 'dist/index.js' ], '/opt/bernrodeWeb' );

                    //Give server time to start web server
                    setTimeout( () => {
                        resolve( {
                            ip: node.ip,
                            id: vm     ,
                        } );
                    }, 5000 );
                } );
            } );
        }

        async function createNGINXConf( ips ) {
            if( await new Promise( ( resolve ) => fs.exists( path.join( __dirname, 'nginx.conf' ), ( exists ) => resolve( exists ) ) ) )
                await new Promise( ( resolve, reject ) => fs.unlink( path.join( __dirname, 'nginx.conf' ), ( err ) => err ? reject( err ) : resolve() ) );

            var nginxConf = `
worker_processes  1;


events {
    worker_connections  1024;
}


http {
    upstream backend {
        server ${ips[0]}:3001;  
        server ${ips[1]}:3001; 
        server ${ips[2]}:3001;  
    }

    server {
        listen 80; 

        location / {
            proxy_pass http://backend;
        }
    }
}`;
            await new Promise( ( resolve, reject ) => fs.writeFile( path.join( __dirname, 'nginx.conf' ), nginxConf, ( err ) => err ? reject( err ) : resolve() ) );
        }

        return new Promise( async ( resolve ) => {
            var result = await Promise.all( [ createNode( 1 ), createNode( 2 ), createNode( 3 ) ] );

            createNGINXConf( result.map( ( r ) => r.ip ) );

            var nginx = await proxmox.cloneVM( 207, 'BAREBONES-LB', true );

            await proxmox.startVM( nginx );

            const runner = require( '../../src/runner' );

            runner.awaitNode( await proxmox.getMACAddress( nginx ), async ( node ) => {
                await node.executeCommand( 'pacman', [ '-S', '--noconfirm', 'nginx-mainline' ] );
                await node.executeCommand( 'rm', [ '/etc/nginx/nginx.conf' ] );
                await node.transmitFile  ( path.join( __dirname, './nginx.conf' )              , '/etc/nginx/nginx.conf' );
                await node.executeCommand( 'nginx' );

                //Give server time to start web server
                setTimeout( () => {
                    resolve( {
                        NODE_IPS: [ node.ip ],
                        NODE_IDS: [ ...result.map( ( r ) => r.id ), nginx ],
                        PORT: 80,
                    } );
                }, 5000 );
            } );
        } );
    },
    down: async function( proxmox, vars ) {
        for( var id of vars.NODE_IDS ) {
            await proxmox.stopVM  ( id );
            await proxmox.deleteVM( id );
        }
    },
    createImage: function( proxmox, vars ) {
        console.log( 'SETUP IMAGE' );
    },
    restoreImage: function( proxmox, vars ) {
        console.log( 'SETUP RESTORE IMAGE' );
    },
}