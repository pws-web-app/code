#!/bin/bash

cd /opt/bernrodeWeb
npm install
npm run compile
pacman -Sy
pacman -S --noconfirm mariadb
mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
systemctl start mysqld

mysql -e "SET PASSWORD = PASSWORD('4UxZ#^1^')"
mysql -e "DROP USER ''@'localhost'"
mysql -e "DROP USER ''@'$(hostname)'"
mysql -e "DROP DATABASE test"
mysql -e "CREATE DATABASE PWS"
mysql -e "FLUSH PRIVILEGES"