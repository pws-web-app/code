#!/bin/sh

#ARGUMENTS:
# 1: peer 1 IP
# 2: peer 1 hostname
# 3: own hostname

#ON ALL INSTANCES:
hwclock --adjust

echo -e "$3" > /etc/hostname
hostnamectl --transient set-hostname $3
echo -e "127.0.0.1\tlocalhost\n::1\t\tlocalhost\n127.0.1.1\t$3.localdomain\t$3" > /etc/hosts
echo -e "$1\t$2" >> /etc/hosts
