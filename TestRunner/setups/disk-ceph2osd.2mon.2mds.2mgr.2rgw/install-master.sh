#!/bin/sh

#ARGUMENTS:
# 1: fsid
# 2: mon 1 hostname
# 3: mon 1 IP
# 4: mon 2 hostname
# 5: mon 2 IP
# 6: mon 3 hostname
# 7: mon 3 IP

#ON MASTER MON INSTANCE:
ceph-authtool --create-keyring /tmp/ceph.mon.keyring --gen-key -n mon. --cap mon 'allow *'
ceph-authtool --create-keyring /etc/ceph/ceph.client.admin.keyring --gen-key -n client.admin --cap mon 'allow *' --cap osd 'allow *' --cap mds 'allow *' --cap mgr 'allow *'
ceph-authtool --create-keyring /var/lib/ceph/bootstrap-osd/ceph.keyring --gen-key -n client.bootstrap-osd --cap mon 'profile bootstrap-osd' --cap mgr 'allow r'
ceph-authtool /tmp/ceph.mon.keyring --import-keyring /etc/ceph/ceph.client.admin.keyring
ceph-authtool /tmp/ceph.mon.keyring --import-keyring /var/lib/ceph/bootstrap-osd/ceph.keyring
chown ceph:ceph /tmp/ceph.mon.keyring

monmaptool --create --add $2 $3 --add $4 $5 --add $6 $7 --fsid $1 /tmp/monmap
