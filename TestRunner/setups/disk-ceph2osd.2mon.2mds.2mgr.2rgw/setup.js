const fs = require( 'fs' );
const path = require( 'path' );

module.exports = {

    up: function( proxmox, vars ) {
        return new Promise( async ( resolve ) => {

            const runner = require( '../../src/runner' );

			const template = 203; //TODO
			const clones   = 2  ;

			var mgrvm;
			var vms = [];

			console.log( `CLONING ${clones + 1} VMs..` );
			for( var i = 0; i < clones; ++i )
				vms.push( await proxmox.cloneVM( template, `CEPH${clones}-${i}`, false ) );
			mgrvm = await proxmox.cloneVM( template, `CEPH${clones}-M`, false );
			console.log( `CLONING COMPLETE` );

			console.log( `STARTING VMs..` );
			for( var vm of vms )
				await proxmox.startVM( vm );
			await proxmox.startVM( mgrvm );
			console.log( `START COMPLETE` );

			var nodes = await Promise.all( vms.map( ( vm ) => new Promise( async ( resolve ) => {
				runner.awaitNode( await proxmox.getMACAddress( vm ), ( node ) => {
					console.log( `VM ${vm} AVAILABLE` );
					resolve( { vmId: vm, node } );
				} );
			} ) ) );

			var mgrnode = await new Promise( async ( resolve ) => {
				runner.awaitNode( await proxmox.getMACAddress( mgrvm ), ( node ) => {
					console.log( `VM M (${mgrvm}) AVAILABLE` );
					resolve( { vmId: mgrvm, node } );
				} );
			} );

			//ALL BUT MASTER
			for( var n of nodes ) {
				var vm = n.vmId;
				console.log( `SETTING UP NODE ${vm}..` );
				var node = n.node;

				var thisvm;
				var args = [ '/tmp/installCeph.sh' ];
				for( var i = 0; i < clones; ++i )
				{
					if( vms[ i ] == vm )
					{
						thisvm = i;
						continue;
					}

					args.push( `${nodes[ i ].node.ip}` );
					args.push( `arch-vm-${i}` );
				}
				args.push( `arch-vm-${thisvm}` )

				await node.transmitFile  ( path.join( __dirname, './install.sh' ), '/tmp/installCeph.sh' );
				await node.executeCommand( '/bin/bash', args, '/tmp' );

				console.log( 'NODE SETUP COMPLETE' );
			};

			//MASTER
			console.log( `SETTING UP MASTER NODE (${mgrvm})..` );

			var thisvm;
			var args = [ '/tmp/installCephMaster.sh' ];
			for( var i = 0; i < clones; ++i )
			{
				args.push( `${nodes[ i ].node.ip}` );
				args.push( `arch-vm-${i}` );
			}
			args.push( `arch-vm-m` )
			args.push( `10.0.11` ); //TODO

			await mgrnode.node.transmitFile  ( path.join( __dirname, './deploy.sh' ), '/tmp/installCephMaster.sh' );
			var output = await mgrnode.node.executeCommand( '/bin/bash', args, '/tmp' );

			console.log( `SETUP DONE` );

			resolve( {
				NODE_IPS: nodes.map( ( n ) => n.node.ip ),
				NODE_IDS: nodes.map( ( n ) => n.vmId ),
				MASTER_IP: mgrnode.node.ip,
				MASTER_ID: mgrvm,
				KEY: output.stdout[ output.stdout.length - 1 ]
			} );
		} );
    },
    down: async function( proxmox, vars ) {
        for( var id of vars.NODE_IDS ) {
            await proxmox.stopVM  ( id );
            await proxmox.deleteVM( id );
        }
    },
    createImage: function( proxmox, vars ) {
        console.log( 'SETUP IMAGE' );
    },
    restoreImage: function( proxmox, vars ) {
        console.log( 'SETUP RESTORE IMAGE' );
    },
}
