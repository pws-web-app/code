const fs = require( 'fs' );
const path = require( 'path' );

module.exports = {

    up: function( proxmox, vars ) {
        return new Promise( async ( resolve ) => {
            const runner = require( '../../src/runner' );

			const template = 201;
			const clones   = 2  ;

			var vms = [];

			console.log( `CLONING ${clones} VMs..` );
			for( var i = 0; i < clones; ++i )
				vms.push( await proxmox.cloneVM( template, `GLUSTER${clones}-${i}`, false ) );
			console.log( `CLONING COMPLETE` );

			console.log( `STARTING VMs..` );
			for( var vm of vms )
				await proxmox.startVM( vm );
			console.log( `START COMPLETE` );

			var nodes = await Promise.all( vms.map( ( vm ) => new Promise( async ( resolve ) => {
				runner.awaitNode( await proxmox.getMACAddress( vm ), ( node ) => {
					console.log( `VM ${vm} AVAILABLE` );
					resolve( { vmId: vm, node } );
				} );
			} ) ) );

			for( var n of nodes ) {
				var vm = n.vmId;
				console.log( `SETTING UP NODE ${vm}..` );
				var node = n.node;

				var thisvm;
				var args = [ '/tmp/installGluster.sh' ];
				for( var i = 0; i < clones; ++i )
				{
					if( vms[ i ] == vm )
					{
						thisvm = i;
						continue;
					}

					args.push( `${nodes[ i ].node.ip}` );
					args.push( `${i}` );
				}
				args.push( `${thisvm}` );

				await node.transmitFile  ( path.join( __dirname, './install.sh' ), '/tmp/installGluster.sh' );
				await node.executeCommand( '/bin/bash', args, '/tmp' );

				console.log( 'NODE SETUP COMPLETE' );
			};

			console.log( `SETTING UP MASTER (${vms[ 0 ]})..` );
			await nodes[ 0 ].node.transmitFile  ( path.join( __dirname, './install-master.sh' ), '/tmp/installGlusterMaster.sh' );
			await nodes[ 0 ].node.executeCommand( '/bin/bash', [ `/tmp/installGlusterMaster.sh` ], '/tmp' );
			console.log( `SETUP DONE` );
			
			resolve( {
				NODE_IPS: nodes.map( ( n ) => n.node.ip ),
				NODE_IDS: nodes.map( ( n ) => n.vmId ),
				MASTER_IP: nodes[ 0 ].node.ip,
				MASTER_ID: vms[ 0 ]
			} );
        } );
    },
    down: async function( proxmox, vars ) {
        for( var id of vars.NODE_IDS ) {
            await proxmox.stopVM  ( id );
            await proxmox.deleteVM( id );
        }
    },
    createImage: function( proxmox, vars ) {
        console.log( 'SETUP IMAGE' );
    },
    restoreImage: function( proxmox, vars ) {
        console.log( 'SETUP RESTORE IMAGE' );
    },
}
