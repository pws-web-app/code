#!/bin/sh

#ARGUMENTS:
#  1: peer 1 IP
#  2: peer 1 ID
#  3: host ID

#ON ALL INSTANCES:
echo -e "127.0.0.1\tlocalhost\n::1\t\tlocalhost\n127.0.1.1\tarch-vm-$3.localdomain\tarch-vm-$3" > /etc/hosts
echo -e "$1\tarch-vm-$2" >> /etc/hosts
