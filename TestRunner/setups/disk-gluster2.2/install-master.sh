#!/bin/sh

#ON arch-vm-0 INSTANCE:
gluster peer probe arch-vm-1
gluster volume create pws replica 2 arch-vm-0:/gluster/0/brick arch-vm-1:/gluster/0/brick
gluster volume start pws

#TO CONNECT TO THE DRIVE
mount -t glusterfs localhost:/pws /mnt
