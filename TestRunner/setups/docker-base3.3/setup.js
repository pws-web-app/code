const fs = require( 'fs' );
const path = require( 'path' );

const NODE_COUNT = 3;

module.exports = {
    up: function( proxmox, vars ) {
        return new Promise( async ( resolve ) => {
            const runner = require( '../../src/runner' );

            var promises = [];
            for( let i = 0; i < NODE_COUNT; i++ ) {
                promises.push( new Promise( async ( resolve ) => {
                    var vm = await proxmox.cloneVM( 200, 'DOCKER' + (i + 1), true );

                    await proxmox.startVM( vm );

                    runner.awaitNode( await proxmox.getMACAddress( vm ), async ( node ) => {
                        console.log( 'NODE AVAILABLE' );
                        
                        await node.transmitFile  ( path.join( __dirname, './install.sh' )              , '/tmp/installDocker.sh' );
                        await node.transmitFile  ( path.join( __dirname, './docker-compose.yml' )      , '/opt/bernrodeWeb/docker-compose.yml' );
        
                        resolve( {
                            node: node,
                            vm: vm,
                        } );
        
                    } );
                } ) );
            }

            var data = await Promise.all( promises );

            var master = data[ 0 ];

            var output = await master.node.executeCommand( 'docker', [ 'swarm', 'init' ] );
            var stdout = output.stdout.join( '\n' );
            var regex = new RegExp( /docker swarm join --token ([A-Za-z0-9-]+) [0-9\.]+:2377/g );
            var params = regex.exec( stdout );
            if( params[ 1 ] == null )
                throw new Error( `No token found in stdout: ${stdout}` );

            var token = params[ 1 ];

            for( let i = 1; i < NODE_COUNT; i++ ) {
                await data[ i ].node.executeCommand( 'docker', [ 'swarm', 'join', '--token', token, `${master.node.ip}:2377` ] );
            }

            await master.node.executeCommand( '/bin/bash', [ '/tmp/installDocker.sh' ], '/tmp' );

            await runner.httpAvailable( `http://${master.node.ip}:3001` );

            resolve( {
                NODE_IPS: [ master.node.ip ],
                NODE_IDS: data.map( ( i ) => i.vm ),
                PORT    : 3001,
            } );
        } );
    },
    down: async function( proxmox, vars ) {
        for( var id of vars.NODE_IDS ) {
            await proxmox.stopVM  ( id );
            await proxmox.deleteVM( id );
        }
    },
    createImage: function( proxmox, vars ) {
        console.log( 'SETUP IMAGE' );
    },
    restoreImage: function( proxmox, vars ) {
        console.log( 'SETUP RESTORE IMAGE' );
    },
}