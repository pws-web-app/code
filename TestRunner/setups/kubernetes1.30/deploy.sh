#!/bin/bash
#ARG 1 = MASTER IP
#ARG 2 = REPLICAS

kubectl run bernrodeweb --image=tomvanliempd/bernrodeweb --port=3001 --image-pull-policy=Never --env="DB_HOST=$1" --replicas=$2

kubectl expose deployment bernrodeweb --type=NodePort

kubectl describe service bernrodeweb