const fs = require( 'fs' );
const path = require( 'path' );

module.exports = {
    up: function( proxmox, vars ) {
        return new Promise( async ( resolve ) => {
            const runner = require( '../../src/runner' );

            var vm = await proxmox.cloneVM( 200, 'DOCKER1', true );

            await proxmox.startVM( vm );

            console.log( 'STARTED' );
            runner.awaitNode( await proxmox.getMACAddress( vm ), async ( node ) => {
                console.log( 'NODE AVAILABLE' );

                await node.transmitFile  ( path.join( __dirname, './install.sh' )              , '/tmp/installDocker.sh' );
                await node.transmitFile  ( path.join( __dirname, './docker-compose.yml' )      , '/opt/bernrodeWeb/docker-compose.yml' );
                await node.executeCommand( '/bin/bash', [ '/tmp/installDocker.sh' ]            , '/tmp' );

                await runner.httpAvailable( `http://${node.ip}:3001` );

                resolve( {
                    NODE_IPS: [ node.ip ],
                    NODE_IDS: [ vm      ],
                    PORT: 3001,
                } );

            } );

        } );
    },
    down: async function( proxmox, vars ) {
        for( var id of vars.NODE_IDS ) {
            await proxmox.stopVM  ( id );
            await proxmox.deleteVM( id );
        }
    },
    createImage: function( proxmox, vars ) {
        console.log( 'SETUP IMAGE' );
    },
    restoreImage: function( proxmox, vars ) {
        console.log( 'SETUP RESTORE IMAGE' );
    },
}