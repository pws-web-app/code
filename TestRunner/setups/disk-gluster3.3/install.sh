#!/bin/sh

#ARGUMENTS:
#  1: peer 1 IP
#  2: peer 1 ID
#  3: peer 2 IP
#  4: peer 2 ID
#  5: host ID

#ON ALL INSTANCES:
echo -e "127.0.0.1\tlocalhost\n::1\t\tlocalhost\n127.0.1.1\tarch-vm-$5.localdomain\tarch-vm-$5" > /etc/hosts
echo -e "$1\tarch-vm-$2\n$3\tarch-vm-$4" >> /etc/hosts
