#!/bin/sh

#ARGUMENTS:
# 1: fsid
# 2: mon 1 hostname
# 3: mon 1 IP
# 4: mon 2 hostname
# 5: mon 2 IP
# 6: mon 3 hostname
# 7: mon 3 IP
# 8: first 3 parts of IP pool (eg. 192.168.0, because .0/24 gets appended)

#ON MON INSTANCE(S):
echo -e "[global]\nfsid = $1\nmon_initial_members = $2, $4, $6\nmon_host = $3, $5, $7" > /etc/ceph/ceph.conf
echo -e "public_network = $8.0/24" >> /etc/ceph/ceph.conf
echo -e "auth_cluster_required = cephx\nauth_service_required = cephx\nauth_client_required = cephx" >> /etc/ceph/ceph.conf
echo -e "osd_journal_size = 1024\nosd_pool_default_size = 3\nosd_pool_default_min_size = 2\nosd_pool_default_pg_num = 333\nosd_pool_default_pgp_num = 333\nosd_crush_chooseleaf_type = 1" >> /etc/ceph/ceph.conf
echo -e "[mds.$2]\nhost = $2\n[mds.$4]\nhost = $4\n[mds.$6]\nhost = $6" >> /etc/ceph/ceph.conf
