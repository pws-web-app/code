#!/bin/sh

#ARGUMENTS:
# 1: peer 1 IP
# 2: peer 1 hostname
# 3: peer 2 IP
# 4: peer 2 hostname
# 5: peer 3 IP
# 6: peer 3 hostname
# 7: peer 4 IP
# 8: peer 4 hostname
# 9: peer 5 IP
# 10: peer 5 hostname
# 11: own hostname

#ON ALL INSTANCES:
hwclock --adjust

echo -e "${11}" > /etc/hostname
hostnamectl --transient set-hostname ${11}
echo -e "127.0.0.1\tlocalhost\n::1\t\tlocalhost\n127.0.1.1\t${11}.localdomain\t${11}" > /etc/hosts
echo -e "$1\t$2\n$3\t$4\n$5\t$6\n$7\t$8\n$9\t${10}" >> /etc/hosts
