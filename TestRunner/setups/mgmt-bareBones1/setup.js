const fs = require( 'fs' );
const path = require( 'path' );

module.exports = {
    up: function( proxmox, vars ) {
        return new Promise( async ( resolve ) => {
            const runner = require( '../../src/runner' );

            var vm = await proxmox.cloneVM( 207, 'BAREBONES1', true );

            await proxmox.startVM( vm );
            
            runner.awaitNode( await proxmox.getMACAddress( vm ), async ( node ) => {
                await node.transmitFolder( path.join( __dirname, '../../projects/bernrodeWeb' ), '/opt/bernrodeWeb' );
                await node.transmitFile  ( path.join( __dirname, './install.sh' )              , '/tmp/installBernrodeWeb.sh' );
                await node.executeCommand( '/bin/bash', [ '/tmp/installBernrodeWeb.sh' ]       , '/opt/bernrodeWeb' );
                
                node.executeCommand( 'node', [ 'dist/index.js' ], '/opt/bernrodeWeb' );

                //Give server time to start web server
                setTimeout( () => {
                    resolve( {
                        NODE_IPS: [ node.ip ],
                        NODE_IDS: [ vm      ],
                        PORT: 3001,
                    } );
                }, 5000 );
            } );

        } );
    },
    down: async function( proxmox, vars ) {
        for( var id of vars.NODE_IDS ) {
            await proxmox.stopVM  ( id );
            await proxmox.deleteVM( id );
        }
    },
    createImage: function( proxmox, vars ) {
        console.log( 'SETUP IMAGE' );
    },
    restoreImage: function( proxmox, vars ) {
        console.log( 'SETUP RESTORE IMAGE' );
    },
}