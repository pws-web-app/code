#!/bin/sh

#ARGUMENTS:
#  1: peer 1 IP
#  2: peer 1 ID
#  3: peer 2 IP
#  4: peer 2 ID
#  5: peer 3 IP
#  6: peer 3 ID
#  7: peer 4 IP
#  8: peer 4 ID
#  9: peer 5 IP
# 10: peer 5 ID
# 11: host ID

#ON ALL INSTANCES:
echo -e "127.0.0.1\tlocalhost\n::1\t\tlocalhost\n127.0.1.1\tarch-vm-${11}.localdomain\tarch-vm-${11}" > /etc/hosts
echo -e "$1\tarch-vm-$2\n$3\tarch-vm-$4\n$5\tarch-vm-$6\n$7\tarch-vm-$8\n$9\tarch-vm-${10}" >> /etc/hosts
