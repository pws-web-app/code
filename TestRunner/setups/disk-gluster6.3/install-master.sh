#!/bin/sh

#ON arch-vm-0 INSTANCE:
gluster peer probe arch-vm-1
gluster peer probe arch-vm-2
gluster peer probe arch-vm-3
gluster peer probe arch-vm-4
gluster peer probe arch-vm-5
gluster volume create pws replica 3 arch-vm-0:/gluster/0/brick arch-vm-1:/gluster/0/brick arch-vm-2:/gluster/0/brick arch-vm-3:/gluster/0/brick arch-vm-4:/gluster/0/brick arch-vm-5:/gluster/0/brick
gluster volume start pws

#TO CONNECT TO THE DRIVE
mount -t glusterfs localhost:/pws /mnt
