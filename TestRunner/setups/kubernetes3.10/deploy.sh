#!/bin/bash
#ARG 1 = MASTER IP
#ARG 2 = REPLICAS

kubectl run bernrodeweb --image=tomvanliempd/bernrodeweb --port=3001 --image-pull-policy=Never --env="DB_HOST=$1" --replicas=$2

kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/mandatory.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/baremetal/service-nodeport.yaml

cat > /tmp/ingress.yml << EOF
apiVersion: networking.k8s.io/v1beta1 # for versions before 1.14 use extensions/v1beta1
kind: Ingress
metadata:
  name: example-ingress
spec:
  rules:
  - http:
      paths:
      - path: /
        backend:
          serviceName: bernrodeweb
          servicePort: 3001
EOF

kubectl expose deployment bernrodeweb --type=NodePort
kubectl apply -f /tmp/ingress.yml

kubectl describe service ingress-nginx --namespace=ingress-nginx