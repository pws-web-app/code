#!/bin/sh

#ARGUMENTS:
# 1: own hostname
# 2: own index

#ON MON INSTANCE(S) AFTER INSTALL-MASTER:
sudo -u ceph mkdir /var/lib/ceph/mon/ceph-$1
sudo -u ceph ceph-mon --mkfs -i $1 --monmap /tmp/monmap --keyring /tmp/ceph.mon.keyring

systemctl enable ceph-mon@$1
systemctl restart ceph-mon@$1

sudo -u ceph mkdir /var/lib/ceph/mgr
sudo -u ceph mkdir /var/lib/ceph/mgr/ceph-$1
ceph auth get-or-create mgr.$1 mon 'allow profile mgr' osd 'allow *' mds 'allow *' > /var/lib/ceph/mgr/ceph-$1/keyring

ceph mon enable-msgr2

systemctl enable ceph-mgr@$1
systemctl restart ceph-mgr@$1

ceph-volume lvm create --data /dev/sdb
systemctl enable ceph-osd@$2
systemctl restart ceph-osd@$2

sudo -u ceph mkdir /var/lib/ceph/mds/ceph-$2 -p
ceph-authtool --create-keyring /var/lib/ceph/mds/ceph-$2/keyring --gen-key -n mds.$1
ceph auth add mds.$1 osd "allow rwx" mds "allow" mon "allow profile mds" -i /var/lib/ceph/mds/ceph-$1/keyring
systemctl enable ceph-mds@$1
systemctl restart ceph-mds@$1

systemctl restart ceph-mon@$1
systemctl restart ceph-mgr@$1
systemctl restart ceph-mds@$1
systemctl restart ceph-osd@$2
