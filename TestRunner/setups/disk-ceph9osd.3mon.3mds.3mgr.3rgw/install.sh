#!/bin/sh

#ARGUMENTS:
# 1: peer 1 IP
# 2: peer 1 hostname
# 3: peer 2 IP
# 4: peer 2 hostname
# 5: peer 3 IP
# 6: peer 3 hostname
# 7: peer 4 IP
# 8: peer 4 hostname
# 9: peer 5 IP
# 10: peer 5 hostname
# 11: peer 6 IP
# 12: peer 6 hostname
# 13: peer 7 IP
# 14: peer 7 hostname
# 15: peer 8 IP
# 16: peer 8 hostname
# 17: own hostname

#ON ALL INSTANCES:
hwclock --adjust

echo -e "${17}" > /etc/hostname
hostnamectl --transient set-hostname ${17}
echo -e "127.0.0.1\tlocalhost\n::1\t\tlocalhost\n127.0.1.1\t${17}.localdomain\t${17}" > /etc/hosts
echo -e "$1\t$2\n$3\t$4\n$5\t$6\n$7\t$8\n$9\t${10}\n${11}\t${12}\n${13}\t${14}\n${15}\t${16}" >> /etc/hosts
