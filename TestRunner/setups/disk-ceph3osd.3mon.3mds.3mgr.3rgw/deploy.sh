#!/bin/sh

#ARGUMENTS:
# 1: node 1 IP
# 2: node 1 hostname
# 3: node 2 IP
# 4: node 2 hostname
# 5: node 3 IP
# 6: node 3 hostname
# 7: own hostname
# 8: first 3 parts of IP range (eg. 192.168.1)


#ON 1 SEPARATE MASTER NODE:
#ON ALL INSTANCES:
hwclock --adjust

echo -e "$7" > /etc/hostname
echo -e "127.0.0.1\tlocalhost\n::1\t\tlocalhost\n127.0.1.1\t$7.localdomain\t$7" > /etc/hosts
echo -e "$1\t$2\n$3\t$4\n$5\t$6" >> /etc/hosts

echo -e "StrictHostKeyChecking no" >> /etc/ssh/ssh_config
mkdir cluster
cd cluster
ceph-deploy --username pws new $2 $4 $6
ceph-deploy --username pws install $2 $4 $6
echo -e "public_network = $8.0/24" >> ceph.conf
ceph-deploy --username pws mon create-initial
ceph-deploy --username pws admin $2 $4 $6
ceph-deploy --username pws mgr create $2 $4 $6
ceph-deploy --username pws osd create --data /dev/sdb $2
ceph-deploy --username pws osd create --data /dev/sdb $4
ceph-deploy --username pws osd create --data /dev/sdb $6
ssh pws@arch-vm-0 sudo mkdir /var/lib/ceph/mds
ssh pws@arch-vm-1 sudo mkdir /var/lib/ceph/mds
ssh pws@arch-vm-2 sudo mkdir /var/lib/ceph/mds
ceph-deploy --username pws mds create $2 $4 $6
ceph-deploy --username pws rgw create $2 $4 $6
ssh pws@arch-vm-0 sudo ceph osd pool create data 100 100
ssh pws@arch-vm-0 sudo ceph osd pool create metadata 100 100
ssh pws@arch-vm-0 sudo ceph fs new pws metadata data
key=$(cat ceph.client.admin.keyring | grep key | cut -d " " -f3-)
echo $key
#TO MOUNT ON CLIENT:
#mount -t ceph NODE_HOSTNAME:6789:/ MOUNT_PATH -o name=admin,secretfile=PART_TO_SECRET_FILE
