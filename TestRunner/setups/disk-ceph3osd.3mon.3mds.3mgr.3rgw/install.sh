#!/bin/sh

#ARGUMENTS:
# 1: peer 1 IP
# 2: peer 1 hostname
# 3: peer 2 IP
# 4: peer 2 hostname
# 5: own hostname

#ON ALL INSTANCES:
hwclock --adjust

echo -e "$5" > /etc/hostname
hostnamectl --transient set-hostname $5
echo -e "127.0.0.1\tlocalhost\n::1\t\tlocalhost\n127.0.1.1\t$5.localdomain\t$5" > /etc/hosts
echo -e "$1\t$2\n$3\t$4" >> /etc/hosts
