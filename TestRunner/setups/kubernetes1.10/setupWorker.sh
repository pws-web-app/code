#!/bin/bash
#TODO ARG 1 = MASTER IP
#TODO ARG 2 = TOKEN
#TODO ARG 3 = HASH
#TODO ARG 4 = WORKER INDEX

export CNI_VERSION="v0.6.0"
mkdir -p /opt/cni/bin
curl -L "https://github.com/containernetworking/plugins/releases/download/${CNI_VERSION}/cni-plugins-amd64-${CNI_VERSION}.tgz" | tar -C /opt/cni/bin -xz

hostnamectl set-hostname KUBERNETES-$4

cat > /etc/hosts << EOF
127.0.0.1       localhost
::1             localhost
$1              arch-base
127.0.1.1       KUBERNETES-$4.localdomain   KUBERNETES-$4
EOF

kubeadm join --token $2 $1:6443 --discovery-token-ca-cert-hash sha256:$3

sleep 90