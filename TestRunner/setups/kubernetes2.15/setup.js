const fs = require( 'fs' );
const path = require( 'path' );

const WORKER_COUNT = 2;
const REPLICA_COUNT = 15;

module.exports = {
    up: function( proxmox, vars ) {
        return new Promise( async ( resolve ) => {
            const runner = require( '../../src/runner' );

            var promises = [];
            for( let i = 0; i < ( WORKER_COUNT + 1 ); i++ ) {
                promises.push( new Promise( async ( resolve ) => {
                    var vm = await proxmox.cloneVM( 206, 'KUBERNETES' + i, true );

                    await proxmox.startVM( vm );

                    runner.awaitNode( await proxmox.getMACAddress( vm ), async ( node ) => {
                        console.log( 'NODE AVAILABLE' );

                        resolve( {
                            node: node,
                            vm: vm,
                        } );
                    } );
                } ) );
            }


            var vms = await Promise.all( promises );

            var master = vms[ 0 ].node;

            await master.transmitFile( path.join( __dirname, './flannel.yaml' ), '/tmp/flannel.yaml' );
            await master.transmitFile( path.join( __dirname, './setupMaster.sh' ), '/tmp/setupMaster.sh' );
            await master.transmitFile( path.join( __dirname, './deploy.sh' ), '/tmp/deploy.sh' );

            console.log( 'SETUP MASTER' );

            var output = await master.executeCommand( '/bin/bash', [ '/tmp/setupMaster.sh' ] );
            var stdout = output.stdout.join( '\n' );
            var regex = new RegExp( /kubeadm join [0-9\.]+:6443 --token ([A-Za-z0-9.]+) \\\n    --discovery-token-ca-cert-hash sha256:([A-Za-z0-9]+)/g );
            var params = regex.exec( stdout );
            if( params[ 1 ] == null || params[ 2 ] == null )
                throw new Error( `No token found in stdout: ${stdout}` );

            var token = params[ 1 ];
            var hash = params[ 2 ];

            console.log( 'RECEIVE TOKEN AND HASH', token, hash );

            var ips = [];

            for( let i = 1; i < ( WORKER_COUNT + 1 ); i++ ) {
                console.log( 'SETUP WORKER', i );
                await vms[ i ].node.transmitFile( path.join( __dirname, './setupWorker.sh' ), '/tmp/setupWorker.sh' );
                await vms[ i ].node.executeCommand( '/bin/bash', [ '/tmp/setupWorker.sh', master.ip, token, hash, i ] );

                ips.push( vms[ i ].node.ip );
            }

            console.log( 'DEPLOYING' );

            var output2 = await master.executeCommand( '/bin/bash', [ '/tmp/deploy.sh', master.ip, REPLICA_COUNT ] );
            var stdout2 = output2.stdout.join( '\n' );
            console.log( stdout2 );
            var regex2 = new RegExp( /NodePort: +http +([0-9]+)\/TCP/g );
            var params2 = regex2.exec( stdout2 );
            console.log( params2 );
            if( params2[ 1 ] == null )
                throw new Error( `No port found in stdout: ${stdout}` );

            var port = params2[ 1 ];

            console.log( 'WAITING HTTP' );

            await runner.httpAvailable( `http://${master.ip}:${port}` );

            console.log( 'SETUP COMPLETE' );

            resolve( {
                NODE_IPS: [ master.ip ],
                NODE_IDS: vms.map( ( i ) => i.vm ),
                PORT: port,
            } );
        } );
    },
    down: async function( proxmox, vars ) {
        for( var id of vars.NODE_IDS ) {
            await proxmox.stopVM( id );
            await proxmox.deleteVM( id );
        }
    },
    createImage: function( proxmox, vars ) {
        console.log( 'SETUP IMAGE' );
    },
    restoreImage: function( proxmox, vars ) {
        console.log( 'SETUP RESTORE IMAGE' );
    },
}