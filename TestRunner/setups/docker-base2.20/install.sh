#!/bin/bash

systemctl start mysqld

cd /opt/bernrodeWeb

DOCKERHOST=$(ip -4 addr show ens18 | grep -oP '(?<=inet\s)\d+(\.\d+){3}') docker stack deploy --compose-file docker-compose.yml bernrodeWeb