#!/bin/bash

pacman -S socat curl ebtables ethtool wget unzip

curl https://download.docker.com/linux/static/stable/x86_64/docker-18.09.9.tgz -o docker-18.09.9.tgz
tar -xzvf docker-18.09.9.tgz
cp docker/* /usr/bin/

systemctl enable docker && systemctl start docker
sysctl net.bridge.bridge-nf-call-iptables=1

RELEASE="$(curl -sSL https://dl.k8s.io/release/stable.txt)"

mkdir -p /opt/bin
cd /opt/bin
curl -L --remote-name-all https://storage.googleapis.com/kubernetes-release/release/${RELEASE}/bin/linux/amd64/{kubeadm,kubelet,kubectl}
chmod +x {kubeadm,kubelet,kubectl}

curl -sSL "https://raw.githubusercontent.com/kubernetes/kubernetes/${RELEASE}/build/debs/kubelet.service" | sed "s:/usr/bin:/opt/bin:g" > /etc/systemd/system/kubelet.service
mkdir -p /etc/systemd/system/kubelet.service.d
curl -sSL "https://raw.githubusercontent.com/kubernetes/kubernetes/${RELEASE}/build/debs/10-kubeadm.conf" | sed "s:/usr/bin:/opt/bin:g" > /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
systemctl enable kubelet && systemctl start kubelet

ln -s /opt/bin/kubelet /usr/bin/kubelet
ln -s /opt/bin/kubectl /usr/bin/kubectl
ln -s /opt/bin/kubeadm /usr/bin/kubeadm

kubeadm config images pull

cd /opt/bernrodeWeb
docker build -t tomvanliempd/bernrodeweb .