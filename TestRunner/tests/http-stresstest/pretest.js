module.exports = function( proxmox, vars ) {
    return {
        DURATION   : vars.stresstime + "s",
        CONNECTIONS: 600,
        THREADS    : 20,
        SERVER_URL : `http://${vars.NODE_IPS[0]}:${vars.PORT}`,
    }
}