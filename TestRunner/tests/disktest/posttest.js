module.exports = {
    onStdout   : null,
    onStderr   : null,
    onTestStart: null,
    postProcess: function( stdout, stderr, vars ) {
        if( stderr != null )
            throw new Error( 'Unable to process test because of errors: ' + stderr );

        try {
            var json = stdout.split( '----' )[ 1 ];
            var result = JSON.parse( json );
        } catch( e ) {
            throw new Error( 'Unable to parse test result:' + e );
        }

        console.log( stdout );

        return result;
    }
}