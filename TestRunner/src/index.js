global.config = require( '../runnerConfig.json' );

const tests   = require( './tests'  );
const setups  = require( './setups' );
const runner  = require( './runner' );

const express    = require( 'express' );
const path       = require( 'path'    );
const bodyParser = require( 'body-parser' );

( async () => {

    runner.addLogListener( ( item ) => {
        var params = [];
        Object.keys( item.params ).forEach( ( k ) => {
            params.push( k );
            params.push( '=' );
            if( Array.isArray( item.params[ k ] ) )
                params.push( `[Array ${item.params[ k ].length}]` )
            else if( item.params[ k ] == null )
                params.push( `[null]` );
            else if( typeof item.params[ k ] == 'object')
                params.push( `[Object]` );
            else
                params.push( item.params[ k ] );
            params.push( ';' );
        } );
        params.unshift( '||  ' );

        console.log( ( `(${item.time.toLocaleString( 'nl-NL' )}) - ${item.event}` ).padEnd( 70, ' ' ), ...params );
    } );

    runner.log( 'Test indexing finished', { tests: tests, setups: setups } );
    
    const proxmox = require( './proxmox' )( {
        url            : config.proxmox.url,
        user           : config.proxmox.user,
        password       : config.proxmox.password,
        node           : config.proxmox.node,
        templateStorage: config.proxmox.templateStorage,
    } );

    //await proxmox.status();
    runner.log( 'Proxmox connection established' );

    runner.proxmox = proxmox;

    let app = express();
    require( 'express-ws' )( app );

    app.set( 'views', path.join( __dirname, './views' ) );
    app.set( 'view engine', 'ejs' );

    app.use( bodyParser.json() );

    app.use( express.static( path.join( __dirname, '../public' ) ) );

    app.use( '/', require( './routes' ) );

    app.listen( global.config.httpServerPort, () => runner.log( 'HTTP Server started', { port: global.config.httpServerPort } ) );
} )();

