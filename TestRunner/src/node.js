const JSZip = require( 'jszip' );
const fs    = require( 'fs'    );
const path  = require( 'path'  );

module.exports = class Node {

    constructor( ws, ip ) {
        /**
         * @type WebSocket
         */
        this.ws = ws;

        this.ip = ip;

        this._msgId = 0;
        this._responseResolvers = {};

        this.ws.on( 'message', ( msg ) => {
            var data = JSON.parse( msg );
            if( data.msgId != null && this._responseResolvers[ data.msgId ] != null ) {
                this._responseResolvers[ data.msgId ]( msg );
                delete this._responseResolvers[ data.msgId ];
            }
        } );
    }

    _getResponseID() {
        this._msgId++;
        return this._msgId;
    }

    _awaitResponse( msgId, callback ) {
        this._responseResolvers[ msgId ] = callback;
    }

    executeCommand( exec, args, cwd = null ) {
        return new Promise( ( resolve ) => {

            var msgId = this._getResponseID();
            this.ws.send( JSON.stringify( {
                event: 'EXECUTE_COMMAND',
                msgId,
                executable: exec,
                arguments : args,
                cwd       : cwd ,
            } ) );

            this._awaitResponse( msgId, ( msg ) => {
                var data = JSON.parse( msg );
                resolve( {
                    stdout  : data.stdout,
                    stderr  : data.stderr,
                    exitCode: data.exitCode,
                } );
            } );

        } );
    }

    _generateFileData( content, metaData ) {
        var fileBuffer     = Buffer.from( content.toString( 'hex' ) );
        var metaDataBuffer = Buffer.from( JSON.stringify( metaData ) );
        var metaDataLength = Buffer.from( metaDataBuffer.length.toString().padStart( 6, '0' ) );

        return Buffer.concat( [ metaDataLength, metaDataBuffer, fileBuffer ] );
    }

    async transmitFile( srcPath, destPath ) {
        var fileData = await new Promise( ( resolve, reject ) => fs.readFile( srcPath, ( err, data ) => err ? reject( err ) : resolve( data ) ) );
        
        this.ws.send( this._generateFileData( fileData, {
            destPath: destPath,
            event   : 'TRANSMIT_FILE'
        } ), { binary: true } );
    }

    transmitFolder( srcPath, destPath ) {
        return new Promise( async ( resolve, reject ) => {
            function readDir( dir, zip, root ) {
                return new Promise( ( resolve ) => {
                    fs.readdir( dir, async ( err, list ) => {
                        if( err ) return reject( err );

                        for( let file of list ) {
                            file = path.resolve( dir, file );

                            let stat = await new Promise( ( resolve, reject ) => fs.stat( file, ( err, stats ) => err ? reject( err ) : resolve( stats ) ) );

                            if( stat && stat.isDirectory() ) {
                                await readDir( file, zip, root )
                            } else {
                                let fileData = await new Promise( ( resolve, reject ) => fs.readFile( file, ( err, data ) => err ? reject( err ) : resolve( data ) ) );
                                zip.file( path.relative( root, file ), fileData );
                            }
                        }

                        resolve();
                    } );
                } )
            }

            let zip = new JSZip();
            await readDir( srcPath, zip, srcPath );
            
            let content = await zip.generateAsync( {
                type: 'nodebuffer',
                comment: 'TestServerZip',
                compression: "DEFLATE",
                compressionOptions: {
                    level: 9
                }
            } );

            var msgId = this._getResponseID();
            this.ws.send( this._generateFileData( content, {
                destPath: destPath,
                msgId   : msgId,
                event   : 'TRANSMIT_FOLDER'
            } ), { binary: true } );

            this._awaitResponse( msgId, ( msg ) => {
                var data = JSON.parse( msg );
                if( data.err == null )
                    resolve();
                else
                    reject( data.err );
            } );
        } );

    }

}