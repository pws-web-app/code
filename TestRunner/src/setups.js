const path = require( 'path' );
const fs   = require( 'fs'   );

const SETUPS_DIRECTORY = path.join( __dirname, '../', config.setupsDirectory );

const setupFolders = fs.readdirSync( SETUPS_DIRECTORY );

const defaultSetupConfig = {
    key       : null,
    title     : null,
}

function validateSetup( setupFolder ) {
    if( !fs.existsSync( path.join( SETUPS_DIRECTORY, setupFolder, 'setup.json' ) ) )
        throw new Error( `No setup.json present for setup ${setupFolder}.` );
    if( !fs.existsSync( path.join( SETUPS_DIRECTORY, setupFolder, 'setup.js' ) ) )
        throw new Error( `No setup.js present for setup ${setupFolder}.` );
    
    var metaData = require( path.join( SETUPS_DIRECTORY, setupFolder, 'setup.json' ) );

    if( metaData.title == null )
        throw new Error( `setup.json for ${setupFolder} doesn't specify a title` );

    var setup = require( path.join( SETUPS_DIRECTORY, setupFolder, 'setup.js' ) );

    return { ...defaultSetupConfig, ...metaData, key: setupFolder, setup };
}

module.exports = setupFolders.map( validateSetup );