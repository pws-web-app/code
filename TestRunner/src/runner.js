const { spawn } = require( 'child_process' );
const fs = require( 'fs' );
const path = require( 'path' );
const http = require( 'http' );
const Node = require( './node' );

const setups = require( './setups' );
const tests  = require( './tests'  );

module.exports = new class Runner {

    constructor() {
        //Proxmox is populated in index.js
        this.proxmox  = null;
        this.testBook = null;
        this.tests    = [];
        this.jobs     = [];
        this.running  = false;
        this._logListeners = [];
        this._log     = [];
        this._nodes   = {};
        this._nodePromise = {};
    }

    isRunning() {
        return this.running;
    }

    startTests( testBook ) {
        this.testBook = testBook;
        this.running  = true;

        this.testBook.forEach( ( t ) => this.tests.push( this._parseURI( t ) ) );

        this.log( 'Starting tests...', { testBook: this.testBook } );

        this.tests = this.tests.sort( ( a, b ) => {
            if( a.setup > b.setup )
                return 1;
            else if( a.setup < b.setup )
                return -1;
            
            if( a.test > b.test )
                return 1;
            else if( a.test < b.test )
                return -1;
            
            if( a.subtest > b.subtest )
                return 1;
            else if( a.subtest < b.subtest )
                return -1;

            for( var key in Object.keys( a.variables ) ) {
                if( b.variables[ key ] != null ) {
                    if( a.variables[ key ] > b.variables[ key ] )
                        return -1;
                    else if( a.variables[ key ] < b.variables[ key ] )
                        return 1;
                } else {
                    return -1;
                }
            }

            for( var key in Object.keys( b.variables ) ) {
                if( a.variables[ key ] == null ) {
                    return 1;
                }
            }

            return 0;
        } );

        function getSetup( key ) {
            var match = setups.filter( ( s ) => s.key == key );
            if( match.length == 0 )
                throw new Error( `No setup found for key: ${key}` );
            return match[ 0 ];
        }

        function getTest( key ) {
            var match = tests.filter( ( s ) => s.key == key );
            if( match.length == 0 )
                throw new Error( `No test found for key: ${key}` );
            return match[ 0 ];
        }

        var currentSetup = null;
        for( var uriData of this.tests ) {
            var setup = getSetup( uriData.setup );
            var test  = getTest ( uriData.test ) ;

            if( setup != currentSetup && currentSetup != null ) {
                this.jobs.push(
                    {
                        type: 'SETUP_DOWN',
                        setup: currentSetup,
                    }
                );
            }
            if( setup != currentSetup ) {
                this.jobs.push(
                    {
                        type: 'SETUP_UP',
                        setup,
                    }
                );
                currentSetup = setup;
            }
            if( test.clearMode == 'imageRestore' ) {
                //Only create image when doesnt exist already
                this.jobs.push(
                    {
                        type: 'SETUP_CREATE_IMAGE',
                        setup,
                    }
                );
            }
            if( test.pretest != null ) {
                this.jobs.push(
                    {
                        type: 'RUN_PRE_TEST',
                        test,
                        uriData,
                    }
                );
            }
            this.jobs.push(
                {
                    type: 'RUN_TEST',
                    test,
                    uriData,
                }
            );
            this.jobs.push(
                {
                    type: 'RUN_POST_TEST',
                    test,
                    uriData,
                }
            );
            if( test.clearMode == 'imageRestore' ) {
                this.jobs.push(
                    {
                        type: 'SETUP_RESTORE_IMAGE',
                        setup,
                    }
                );
            }
        }

        if( currentSetup != null ) {
            this.jobs.push(
                {
                    type: 'SETUP_DOWN',
                    setup: currentSetup,
                }
            );
        }

        this._runJobs();
    }

    async _runJobs() {
        this.log( `Running ${this.jobs.length} jobs...`, { jobs: this.jobs.map( ( j ) => {
            if( j.setup != null )
                return { type: j.type, setup: j.setup.key };
            else if( j.test != null )
                return { type: j.type, setup: j.test.key };
            else
                return { type: j.type };
        } ) } );

        await this.proxmox.authenticate();

        var vars = {};
        var testVars = null;
        var testResult = null;
        var testStart = null;
        var testEnd = null;
        for( var i = 0; i < this.jobs.length; i++ ) {
            var job = this.jobs[ i ];
            
            switch( job.type ) {
                case 'SETUP_UP': {
                    this.log( `${( i + 1 ).toString().padStart( 3, '0' )}/${this.jobs.length.toString().padStart( 3, '0' )}: SETUP UP`, { setup: job.setup.key } );
                    vars = await job.setup.setup.up( this.proxmox ) || vars;
                    break;
                }
                case 'SETUP_DOWN': {
                    this.log( `${( i + 1 ).toString().padStart( 3, '0' )}/${this.jobs.length.toString().padStart( 3, '0' )}: SETUP DOWN`, { setup: job.setup.key } );
                    await job.setup.setup.down( this.proxmox, vars );
                    vars = {};
                    break;
                }
                case 'SETUP_CREATE_IMAGE': {
                    this.log( `${( i + 1 ).toString().padStart( 3, '0' )}/${this.jobs.length.toString().padStart( 3, '0' )}: SETUP CREATE IMAGE`, { setup: job.setup.key } );
                    vars = await job.setup.setup.createImage( this.proxmox, vars ) || vars;
                    break;
                }
                case 'SETUP_RESTORE_IMAGE': {
                    this.log( `${( i + 1 ).toString().padStart( 3, '0' )}/${this.jobs.length.toString().padStart( 3, '0' )}: SETUP RESTORE IMAGE`, { setup: job.setup.key } );
                    vars = await job.setup.setup.restoreImage( this.proxmox, vars ) || vars;
                    break;
                }
                case 'RUN_PRE_TEST': {
                    this.log( `${( i + 1 ).toString().padStart( 3, '0' )}/${this.jobs.length.toString().padStart( 3, '0' )}: RUN PRETEST`, { test: job.test.key, uri: job.uriData.uri } );
                    testVars = await job.test.pretest( this.proxmox, { ...vars, ...job.uriData.variables } ) || { ...vars, ...job.uriData.variables };
                    testStart = null;
                    testEnd = null;
                    break;
                }
                case 'RUN_TEST': {
                    this.log( `${( i + 1 ).toString().padStart( 3, '0' )}/${this.jobs.length.toString().padStart( 3, '0' )}: RUN TEST`, { test: job.test.key, uri: job.uriData.uri } );
                    if( testVars == null )
                        testVars = { ...vars, ...job.uriData.variables };
                    testStart = new Date();
                    console.log( '(1)' );
                    testResult = await this._startTest( job.test, testVars );
                    testEnd = new Date();
                    break;
                }
                case 'RUN_POST_TEST': {
                    this.log( `${( i + 1 ).toString().padStart( 3, '0' )}/${this.jobs.length.toString().padStart( 3, '0' )}: RUN POST TEST`, { test: job.test.key, uri: job.uriData.uri } );
                    var data = await job.test.posttest.postProcess( testResult.stdout, testResult.stderr, testVars, job.uriData );
                    await this._saveTest( {
                        test     : job.test            ,
                        vars     : testVars            ,
                        data     : data                ,
                        uriData  : job.uriData         ,
                        startTime: testStart           ,
                        endTime  : testEnd             ,
                        resources: testResult.resources,
                    } );
                    testVars = null;
                    testResult = null;
                    await new Promise( ( resolve ) => setTimeout( resolve, 2000 ) );
                    break;
                }
            }
        }
        this.log( `Finished Test Book` );

        this.tests        = [];
        this.jobs         = [];
        this.running      = false;
        this._nodes       = {};
        this._nodePromise = {};
    }

    _startTest( test, testVars ) {
        return new Promise( async ( resolve, reject ) => {
            if( global.testProcess != null )
                throw new Error( 'Previous test process still running.' );
            

            var resourceCollect = await this._startResourcesCollect();

            console.log( '(2)' );
            const p = spawn( test.executable.exec, test.executable.args, {
                env: { ...testVars },
                cwd: test.folder,
            } );

            global.testProcess = p;

            if( test.posttest.onTestStart != null ) {
                test.posttest.onTestStart( p, testVars );
            }
            var stdout = [];
            var stderr = [];

            p.stdout.on( 'data', ( data ) => {
                stdout.push( data );
                if( test.posttest.onStdout != null ) {
                    test.posttest.onStdout( data, testVars );
                }
            } );
            p.stderr.on( 'data', ( data ) => {
                stderr.push( data );
                if( test.posttest.onStderr != null ) {
                    test.posttest.onStderr( data, testVars );
                }
            } );

            p.on( 'exit', async ( code ) => {
                resourceCollect.stop();

                var resources = await resourceCollect.promise;

                global.testProcess = null;
                if( code != 0 )
                    reject( `Process exited with exit code ${code}: ${stderr.map( ( b ) => b.toString() ).join( '\n' )} - ${stdout.map( ( b ) => b.toString() ).join( '\n' )}` );
                else
                    resolve( { exitCode: code, stdout: stdout.length > 0 ? stdout.join( '\n' ) : null, stderr: stderr.length > 0 ? stderr.join( '\n' ) : null, resources } );
            } );
        } );
    }

    async _startResourcesCollect() {

        var resources  = ( await this.proxmox.getResources() ).data;
        var containers = resources.filter( ( r ) => r.type == 'lxc' || r.type == 'qemu' );

        var data = {};
        containers.forEach( ( c ) => {
            data[ c.vmid ] = {
                hostname  : c.name,
                available : {
                    cpu   : c.maxcpu,
                    memory: c.maxmem,
                },
                readings  : [],
            };
        } );

        var shouldStop = false;
        var promises = containers.map( ( container ) => {
            return new Promise( async ( resolve ) => {

                while( !shouldStop ) {
                    var status = {};
                    if( container.type == 'lxc' )
                        status = ( await this.proxmox.statusContainer( container.vmid ) ).data;
                    else if( container.type == 'qemu' )
                        status = ( await this.proxmox.statusVM( container.vmid ) ).data;

                    data[ container.vmid ].readings.push( {
                        time      : new Date().toISOString(),
                        cpu       : status.cpu,
                        memory    : status.mem,
                        diskRead  : status.diskread,
                        diskWrite : status.diskwrite,
                        networkIn : status.netin,
                        networkOut: status.netout,
                    } );

                    await new Promise( ( resolve ) => setTimeout( resolve, 200 ) );
                }

                resolve();
            } );
        } );

        return {
            promise: new Promise( ( resolve ) => Promise.all( promises).then( () => resolve( data ) ) ),
            stop   : () => shouldStop = true,
        }
    }

    async _saveTest( options ) {

        if( !await new Promise( ( resolve ) => fs.exists( path.join( __dirname, '../results' ), ( exists ) => resolve( exists ) ) ) )
            await new Promise( ( resolve, reject ) => fs.mkdir( path.join( __dirname, '../results' ), ( err ) => err ? reject( err ) : resolve() ) );
        
        var fileData = {
            _metaData: {
                setup: options.uriData.setup    ,
                test : options.uriData.test     ,
                vars : options.uriData.variables,

                startTime: options.startTime.toISOString(),
                  endTime: options.  endTime.toISOString(),

                resources: options.resources,
            },
            data: options.data
        };

        await new Promise( ( resolve, reject ) => 
            fs.writeFile( 
                path.join( __dirname, '../results' ) + `/${options.startTime.toISOString()}|${options.uriData.uri}.json`, 
                JSON.stringify( fileData, null, 4 ),
                ( err ) => err ? reject( err ) : resolve()
            ) 
        );
    }

    _parseURI( URI ) {
        var data = {
            setup    : null,
            test     : null,
            subtest  : null,
            variables: {}  ,
            uri      : URI ,
        };

        var parts = URI.split( ':' );
        if( parts.length != 2 )
            throw new Error( `Invalid URI: ${URI}.` );
        
        data.setup = parts[ 0 ];
        
        var parts2 = parts[ 1 ].split( '/' );
        if( parts2.length == 2 ) {
            data.test = parts2[ 0 ];

            var parts3 = parts2[ 1 ].split( '?' );
            if( parts3.length == 2 ) {
                data.subtest = parts3[ 0 ];

                var parts4 = parts3[ 1 ].split( '&' );
                parts4.forEach( ( item ) => {
                    var parts5 = item.split( '=' );
                    if( parts5.length == 2 ) {
                        data.variables[ parts5[ 0 ] ] = parts5[ 1 ];
                    } else {
                        throw new Error( `Invalid variable part [ ${item} ] in URI: ${URI}.` );
                    }
                } );
            } else if( parts3.length == 1 ) {
                data.subtest = parts3[ 0 ];
            }
        } else if( parts2.length == 1 ) {

            var parts3 = parts2[ 0 ].split( '?' );
            if( parts3.length == 2 ) {
                data.test = parts3[ 0 ];

                var parts4 = parts3[ 1 ].split( '&' );
                parts4.forEach( ( item ) => {
                    var parts5 = item.split( '=' );
                    if( parts5.length == 2 ) {
                        data.variables[ parts5[ 0 ] ] = parts5[ 1 ];
                    } else {
                        throw new Error( `Invalid variable part [ ${item} ] in URI: ${URI}.` );
                    }
                } );
            } else if( parts3.length == 1 ) {
                data.test = parts3[ 0 ];
            }
        }

        return data;
    }

    addLogListener( func ) {
        this._logListeners.push( func );
    }

    removeLogListener( func ) {
        this._logListeners = this._logListeners.filter( ( l ) => l != func );
    }

    log( event, params = {} ) {
        var logItem = {
            time  : new Date(),
            event : event,
            params: params,
        };

        this._log.push( logItem );
        this._logListeners.forEach( ( l ) => l( logItem ) );
    }

    getLog() {
        return this._log;
    }

    async nodeConnect( ws, data, ip ) {
        var mac = data.mac.toUpperCase();
        console.log( 'RECEIVED MAC', mac );
        this._nodes[ mac ] = new Node( ws, ip );

        ws.once( 'close', () => {
            delete this._nodes[ mac ];
        } );

        if( this._nodePromise[ mac ] != null ) {
            this._nodePromise[ mac ]( this._nodes[ mac ] );
            delete this._nodePromise[ mac ];
        }
    }

    awaitNode( mac, callback ) {
        console.log( 'AWAITING MAC', mac );
        this._nodePromise[ mac ] = callback;
    }

    async httpAvailable( url ) {
        var available = 0;

        while( available < 16 ) {
            await new Promise( ( resolve ) => setTimeout( resolve, 500 ) );

            console.log( `${available ? '' : 'NOT'} AVAILABLE, Retrying: ${url}` );

            await new Promise( ( resolve ) => {
                var req = http.get( url, {
                    timeout: 200,
                }, ( resp ) => {
                    let data = '';

                    resp.on('data', (chunk) => {
                      data += chunk;
                    });
                  
                    resp.on('end', () => {
                        if( resp.statusCode < 400 ) { //Any 1XX, 2XX or 3XX status code
                            available++;
                            console.log( 'AVAILABLE', available );
                        }
                        resolve();
                    });

                    resp.on( 'error', ( err ) => {
                        console.log( 'Res error', err );
                        available = 0;
                        console.log( 'AVAILABLE', 0 );
                        resolve();
                    })

                } );

                req.on( 'error', ( err ) => {
                    console.log( 'Req error', err );
                    available = 0;
                    console.log( 'AVAILABLE', 0 );
                    resolve();
                } );
            } );
        }
    }
}
