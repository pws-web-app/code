const express = require( 'express' );

var router = express.Router();

const tests  = require( '../tests' );
const setups = require( '../setups' );

router.get( '/', ( req, res ) => {
    res.render( 'test-book-generator', { serverData: JSON.stringify( { tests, setups } ).replace( /'/g, "\\'" ) } );
} );

module.exports = router;