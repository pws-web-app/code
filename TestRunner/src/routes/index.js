const express = require( 'express' );

var router = express.Router();

const runner = require( '../runner' );

router.use( '/test-book', require( './test-book' ) );

var index = 0;
var resI = 0;
router.get( '/', ( req, res ) => {
    var message = null;
    if( runner.isRunning() )
        message = 'Test already started. <a href="/progress">View progress</a>';

    res.render( 'test-start', { message } );
} );

router.post( '/', ( req, res ) => {
    if( runner.isRunning() )
        return res.render( 'test-start', {  message: 'Test already started. <a href="/progress">View progress</a>' } );
    runner.startTests( JSON.parse( req.body.testbook ) );

    res.status( 200 );
} );

router.get( '/progress', ( req, res ) => {
    res.render( 'progress', { log: JSON.stringify( runner.getLog() ) } );
} );

runner.addLogListener( ( item ) => {
    webSockets.forEach( ( ws ) => {
        ws.send(
            JSON.stringify( {
                event: 'LOG_MESSAGE',
                data : item,
            } )
        );
    } );
} );

var webSockets = [];

router.ws( '/progress', ( ws ) => {
    webSockets.push( ws );

    ws.once( 'close', () => webSockets = webSockets.filter( ( w ) => w != ws ) );
} );

router.ws( '/nodeConnect', ( ws, req ) => {
    var ip = req.headers[ 'x-forwarded-for' ] || req.connection.remoteAddress;
    if( ip.substr( 0, 7 ) == "::ffff:" ) {
        ip = ip.substr( 7 )
    }
    ws.once( 'message', ( data ) => {
        var data = JSON.parse( data );
        if( data.event == 'NODE_INIT' )
            runner.nodeConnect( ws, data, ip );
    } );
} );

module.exports = router;