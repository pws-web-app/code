const path = require( 'path' );
const fs   = require( 'fs'   );

const TESTS_DIRECTORY = path.join( __dirname, '../', config.testsDirectory );

const testFolders = fs.readdirSync( TESTS_DIRECTORY );

const defaultTestConfig = {
    title     : null,
    executable: null,
    clearMode : null, //'imageRestore' or 'none'
    subtests  : []  ,
    variables : []  ,
}

const CLEAR_MODES = [ 'imageRestore', 'none' ];

function validateTest( testFolder ) {
    if( !fs.existsSync( path.join( TESTS_DIRECTORY, testFolder, 'test.json' ) ) )
        throw new Error( `No test.json present for test ${testFolder}.` );
    if( !fs.existsSync( path.join( TESTS_DIRECTORY, testFolder, 'posttest.js' ) ) )
        throw new Error( `No posttest.js present for test ${testFolder}.` );
    
    var metaData = require( path.join( TESTS_DIRECTORY, testFolder, 'test.json' ) );

    if( metaData.title == null )
        throw new Error( `test.json for ${testFolder} doesn't specify a title` );
    if( metaData.executable == null )
        throw new Error( `test.json for ${testFolder} doesn't specify an executable` );

    var pretest = null;
    if( fs.existsSync( path.join( TESTS_DIRECTORY, testFolder, 'pretest.js' ) ) ) {
        pretest = require( path.join( TESTS_DIRECTORY, testFolder, 'pretest.js' ) );
    }

    var posttest = require( path.join( TESTS_DIRECTORY, testFolder, 'posttest.js' ) );

    if( !CLEAR_MODES.includes( metaData.clearMode ) )
        throw new Error( `test.json for ${testFolder} doesn't have a valid clear mode. Should be one of ${JSON.stringify( CLEAR_MODES ) } but ${metaData.clearMode} was given.` );

    return { ...defaultTestConfig, ...metaData, key: testFolder, pretest, posttest, folder: path.join( TESTS_DIRECTORY, testFolder ) };
}

module.exports = testFolders.map( validateTest );