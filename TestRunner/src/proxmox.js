const request = require( 'request' );

module.exports = ( options ) => {

    options.url                  = options.url                  || 'https://localhost:8006'              ;
    options.user                 = options.user                 || 'root'                                ;
    options.password             = options.password             || ''                                    ;
    options.node                 = options.node                 || 'pve'                                 ;
    options.net                  = options.net                  || `bridge=vmbr0,name=eth0,ip=dhcp,tag=60`;
    options.templateStorage      = options.templateStorage      || 'local'                               ;
    options.nameserver           = options.nameserver           || '10.0.11.2 8.8.8.8'                   ;

    if( options.url.slice( -1 ) == '/' ) {
        options.url = options.url.slice( 0, -1 );
    }

    const URL          = options.url + '/api2/json'  ;
    const USER         = options.user                ;
    const PASS         = options.password            ;
    const NODE         = options.node                ;
    const STORAGE      = options.templateStorage     ;
    const NET          = options.net                 ;
    const NAMESERVER   = options.nameserver          ;

    let TICKET = '';
    let CSRF   = '';

    function get( path, verb = 'get', data = null, retry = false ) {
        return new Promise( ( resolve, reject ) => {
            request( {
                method   : verb,
                uri      : URL + path,
                form     : data,
                strictSSL: false,
                headers  : {
                    'CSRFPreventionToken': CSRF  ,
                    'Cookie'             : TICKET
                }
            }, ( err, res, body ) => {
                if( err ) {
                    console.log( 'ERROR:', err );
                    return reject( err );
                }

                if( res.statusCode == 401 && !retry ) {
                    authenticate( () => {
                        get( path, verb, data, true ).then( data => {
                            resolve( data );
                        } );
                    } );
                } else {
                    if( res.statusCode == 200 ) {
                        resolve( JSON.parse( body ) );
                    } else {
                        resolve( res.statusMessage + ' - ' + body );
                    }
                }
            } );
        } );
    }

    function authenticate( cb ) {
        getAuthTicket( '/access/ticket' ).then( ( d ) => {
            TICKET = "PVEAuthCookie=" + d.data.ticket;
            CSRF   = d.data.CSRFPreventionToken;
            cb();
        } );
    }

    function getAuthTicket() {
        return new Promise( ( resolve, reject ) => {
            request.post( {
                url      : URL + `/access/ticket?username=${USER}&password=${PASS}`,
                strictSSL: false
            }, ( err, res, body ) => {
                if( err ) {
                    console.log( 'ERROR:', err );
                    return reject( err );
                }

                if( res.statusCode == 200 ) {
                    resolve( JSON.parse( body ) );
                } else {
                    throw new Error( `Auth failed! ${URL + `/access/ticket?username=${USER}&password=${PASS}`}`, body );
                }
            } );
        } );
    }

    var nextIdCallbacks = [];
    function getNextId() {
        return new Promise( async ( resolve ) => {
            async function getID() {
                var res = await get( '/cluster/nextid' );
                var id = Number( res.data );

                resolve( {
                    id,
                    used: () => {
                        nextIdCallbacks.shift();
                        if( nextIdCallbacks.length > 0 )
                            nextIdCallbacks[ 0 ]();
                    }
                } );
            }

            nextIdCallbacks.push( getID );
            if( nextIdCallbacks.length == 1 )
                nextIdCallbacks[ 0 ]();
        } );
    }

    async function waitTask( taskId ) {
        var status;

        while( status != 'stopped' ) {
            var res = await get( `/nodes/${NODE}/tasks/${taskId}/status` );
            status = res.data.status;
            await new Promise( ( resolve ) => setTimeout( resolve, 1000 ) );
        }
    }

    let px = {};

    px.authenticate = () => {
        return new Promise( ( resolve ) => {
            authenticate( resolve );
        } );
    }

    px.status       = () => get( '/nodes'             );
    px.getResources = () => get( '/cluster/resources' );

    px.nodeStatus    = () => get( `/nodes/${NODE}/status`  );
    px.nodeStorage   = () => get( `/nodes/${NODE}/storage` );

    //#region CONTAINERS

    px.listTemplates = () => get( `/nodes/${NODE}/storage/${STORAGE}/content?content=vztmpl` );

    px.deleteContainer  = ( containerId ) => get( `/nodes/${NODE}/lxc/${containerId}`               , 'delete' );
    px.statusContainer  = ( containerId ) => get( `/nodes/${NODE}/lxc/${containerId}/status/current`           );
    px.networkContainer = ( containerId ) => get( `/nodes/${NODE}/lxc/${containerId}/config`                   );
    px.dataContainer    = ( containerId ) => get( `/nodes/${NODE}/lxc/${containerId}/rrddata?timeframe=hour`   );
    px.startContainer   = ( containerId ) => get( `/nodes/${NODE}/lxc/${containerId}/status/start`  , 'post'   );
    px.stopContainer    = ( containerId ) => get( `/nodes/${NODE}/lxc/${containerId}/status/stop`   , 'post'   );

    px.createContainer  = async ( options ) => {
        var id = await getNextId();

        await get( `/nodes/${NODE}/lxc`, 'post', {
            ostemplate  : `local:vztmpl/${options.template}`,
            vmid        : id.id                             ,
            cores       : options.cpu                       ,
            hostname    : options.hostname                  ,
            memory      : options.memory                    ,
            rootfs      : `local-lvm:${options.disk}`       ,
            swap        : options.swap                      ,
            net0        : NET                               ,//OLD: net        : 'bridge=vmbr0,name=eth0,ip=192.168.1.96/24,gw=192.168.1.1,firewall=1',
            password    : PASS                              ,
            unprivileged: 1                                 ,
            nameserver  : NAMESERVER                        ,
            start       : 1                                 ,
        } );

        id.used();

        return id.id;
    }

    px.containerWaitUntilStopped = async ( containerId ) => {
        var stopped = false;
        while( !stopped ) {
            var status = await get( `/nodes/${NODE}/lxc/${containerId}/status/current` );
            if( status != null && status.data != null && status.data.status == 'stopped' )
                stopped = true;
            else
                await new Promise( ( resolve ) => setTimeout( resolve, 1000 ) );
        }
    };

    px.containerWaitUntilStarted = async ( containerId ) => {
        var started = false;
        while( !started ) {
            var status = await get( `/nodes/${NODE}/lxc/${containerId}/status/current` );
            if( status != null && status.data != null && status.data.status == 'running' )
                started = true;
            else
                await new Promise( ( resolve ) => setTimeout( resolve, 1000 ) );
        }
    };

    //#endregion

    //#region VM

    px.cloneVM = async ( templateId, name, linked = false ) => {
        var id = await getNextId();

        var res = await get( `/nodes/${NODE}/qemu/${templateId}/clone`, 'post', {
            newid       : id.id,
            name        : name ,
            target      : NODE ,
            full        : linked ? 0 : 1,
        } );

        id.used();

        await waitTask( res.data );

        return id.id;
    };

    px.startVM = async ( id ) => {
        var res = await get( `/nodes/${NODE}/qemu/${id}/status/start`, 'post' );

        await waitTask( res.data );
    };

    px.stopVM = async ( id ) => {
        var res = await get( `/nodes/${NODE}/qemu/${id}/status/shutdown`, 'post' );

        await waitTask( res.data );
    };
    
    px.deleteVM = async ( id ) => {
        await get( `/nodes/${NODE}/qemu/${id}`, 'delete' );
    };

    px.getMACAddress = async ( id ) => {
        var res = await get( `/nodes/${NODE}/qemu/${id}/config`, 'get' );

        var networkDevices = Object.keys( res.data ).filter( ( key ) => ( /^net[0-9]$/g ).test( key ) );

        if( networkDevices.length != 1 )
            throw new Error( `${networkDevices.length} network devices found on VM ${id}. MAC address could not be fetched.` );
        
        var virtioProperty = res.data[ networkDevices[ 0 ] ].split( ',' ).filter( ( item ) => ( /^virtio=([A-Z0-9]{2}:[A-Z0-9]{2}:[A-Z0-9]{2}:[A-Z0-9]{2}:[A-Z0-9]{2}:[A-Z0-9]{2})$/g ).test( item )  );
        var data = ( /^virtio=([A-Z0-9]{2}:[A-Z0-9]{2}:[A-Z0-9]{2}:[A-Z0-9]{2}:[A-Z0-9]{2}:[A-Z0-9]{2})$/g ).exec( virtioProperty );

        if( data.length != 2 )
            throw new Error( `Invalid virtio property: ${virtioProperty}` );
        return data[ 1 ];
    }

    px.statusVM  = ( id ) => get( `/nodes/${NODE}/qemu/${id}/status/current`           );

    return px;
};