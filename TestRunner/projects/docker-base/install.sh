#!/bin/bash

pacman -S --noconfirm docker mariadb docker-compose
systemctl enable docker
systemctl start docker

cd /opt/bernrodeWeb
docker build -t tomvanliempd/bernrodeweb .

mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
systemctl start mysqld

mysql -e "SET PASSWORD = PASSWORD('4UxZ#^1^')"
mysql -e "DROP USER ''@'localhost'"
mysql -e "DROP USER ''@'$(hostname)'"
mysql -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '4UxZ#^1^';"
mysql -e "DROP DATABASE test"
mysql -e "CREATE DATABASE PWS"
mysql -e "FLUSH PRIVILEGES"

systemctl stop mysqld