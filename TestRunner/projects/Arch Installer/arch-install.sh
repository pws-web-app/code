#!/bin/sh
echo "Starting Arch VM installation.."

echo "Setting time.."
timedatectl set-ntp true

echo ""
echo "Formatting disk.."
echo -e "g\nn\n\n\n+256M\nn\n\n\n+10G\nn\n\n\n\nt\n1\n1\nt\n2\n20\nt\n3\n31\nw\n" | fdisk /dev/sda
mkfs.vfat -F 32 /dev/sda1
mkfs.ext4 /dev/sda2

echo ""
echo "Mounting partitions.."
mount /dev/sda2 /mnt
mkdir /mnt/boot
mount /dev/sda1 /mnt/boot

echo ""
echo "Creating mirror file.."
echo -e "Server = https://mirror.ams1.nl.leaseweb.net/archlinux/\$repo/os/\$arch" > /etc/pacman.d/mirrorlist

echo ""
echo "Installing packages.."
pacstrap /mnt base linux linux-firmware vim nano dhcpcd

echo ""
echo "Generating fstab.."
genfstab -U /mnt >> /mnt/etc/fstab

echo ""
echo "Changing root to new Arch installation.."
mv ./arch-install-2.sh /mnt/install.sh
mv hostname /mnt/hostname
mv ip /mnt/ip
mv router /mnt/router
mv ip_dev /mnt/ip_dev
mv pass /mnt/pass
mv dns /mnt/dns
arch-chroot /mnt ./install.sh
rm /mnt/install.sh
rm /mnt/hostname
rm /mnt/ip
rm /mnt/router
rm /mnt/ip_dev
rm /mnt/dns
rm /mnt/pass

echo ""
echo "Done with installation, rebooting now."
reboot
