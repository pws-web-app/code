#!/bin/sh
echo ""
echo "Continuing installation.."

echo ""
echo "Retrieving variables.."
hostname=$(<hostname)
ip=$(<ip)
router=$(<router)
ip_dev=$(<ip_dev)
pass=$(<pass)
dns=$(<dns)

echo ""
echo "Setting password.."
echo -e "$pass\n$pass\n" | passwd

echo ""
echo "Setting time zone.."
ln -sf /usr/share/zoneinfo/Europe/Amsterdam /etc/localtime

echo ""
echo "Setting hardware clock.."
hwclock --systohc

echo ""
echo "Generating locales.."
echo -e "en_GB.UTF-8 UTF-8\nen_GB ISO-8859-1" > /etc/locale.gen
locale-gen
echo -e "LANG=en_GB.UTF-8" > /etc/locale.conf

echo ""
echo "Configuring network.."
echo -e "$hostname" > /etc/hostname
echo -e "127.0.0.1\tlocalhost\n::1\t\tlocalhost\n$ip\t$hostname.localdomain\t$hostname" > /etc/hosts
echo -e "\ninterface $ip_dev\nstatic routers=$router\nstatic domain_name_servers=$dns" >> /etc/dhcpcd.conf
systemctl enable dhcpcd@$ip_dev.service

echo ""
echo "Making system bootable.."
bootctl --path=/boot install
echo -e "default arch\ntimeout 0" > /boot/loader/loader.conf
echo -e "title\tarch\nlinux\t/vmlinuz-linux\ninitrd\t/initramfs-linux.img\noptions\troot=LABEL=arch_os rw" > /boot/loader/entries/arch.conf
e2label /dev/sda2 "arch_os"

echo ""
echo "Exiting chroot.."
exit
