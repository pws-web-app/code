#!/bin/bash
echo "Please run the following commands on the ISO:"
echo -e "\tsystemctl start sshd\n\tpasswd\n\techo \"PermitRootLogin yes\" >> /etc/ssh/sshd_config"
echo "Also check if the ip_dev file contains the correct network device (check with ip address show)."
echo ""
echo "Please enter the ISO IP address:"
read ip
echo ""
echo "Please enter the password of the ISO:"
read -s pass
echo "Please check if ssh connection is possible. If not, abort and fix the problem before trying again."
ssh root@$ip echo "If you see this message you can continue."
read tmp
echo "Starting copy.."
sshpass -p "$pass" scp ./arch-install.sh root@$ip:/root/arch-install.sh
sshpass -p "$pass" scp ./arch-install-2.sh root@$ip:/root/arch-install-2.sh
sshpass -p "$pass" scp ./hostname root@$ip:/root/hostname
sshpass -p "$pass" scp ./ip root@$ip:/root/ip
sshpass -p "$pass" scp ./router root@$ip:/root/router
sshpass -p "$pass" scp ./ip_dev root@$ip:/root/ip_dev
sshpass -p "$pass" scp ./pass root@$ip:/root/pass
sshpass -p "$pass" scp ./dns root@$ip:/root/dns
sshpass -p "$pass" ssh root@$ip 'chmod +x arch*'
echo ""
echo "Done. Now run ./arch-install.sh from the ISO."
