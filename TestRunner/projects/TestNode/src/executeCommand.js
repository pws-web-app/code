const { spawn } = require( 'child_process' );

module.exports = function( exec, args, cwd = null ) {
    return new Promise( ( resolve ) => {
        const p = spawn( exec, args, {
            cwd: cwd,
        } );

        var stdout = [];
        var stderr = [];

        p.stdout.on( 'data', ( data ) => {
            stdout.push( data.toString() );
        } );
        p.stderr.on( 'data', ( data ) => {
            stderr.push( data.toString() );
        } );

        p.on( 'exit', ( code ) => {
            resolve( { exitCode: code, stdout, stderr } );
        } );
    } );
}