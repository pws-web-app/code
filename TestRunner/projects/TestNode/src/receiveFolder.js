const JSZip = require( 'jszip' );
const path  = require( 'path'  );
const fs    = require( 'fs'    );

module.exports = async function( destPath, zipContent ) {
    if( !await new Promise( ( resolve ) =>
            fs.exists( destPath, ( exists ) => resolve( exists ) )
        ) )
        await new Promise( ( resolve, reject ) => 
            fs.mkdir( destPath, ( err ) => err ? reject( err ) : resolve() ) 
        );

    var zip = new JSZip();

    let contents = await zip.loadAsync( zipContent );
    for( var fileName of Object.keys( contents.files ) ) {
        if( contents.files[ fileName ].dir == true ) {
            await new Promise( ( resolve, reject ) => 
                fs.mkdir( path.resolve( destPath, fileName ), ( err ) => err ? reject( err ) : resolve() ) 
            );
        } else {
            var file = await zip.file( fileName ).async( 'nodebuffer' );

            await new Promise( ( resolve, reject ) =>
                fs.writeFile( path.resolve( destPath, fileName ), file, ( err ) => err ? reject( err ) : resolve() )
            );
        }
    }
}