const TEST_RUNNER_IP = 'testrunner.pws';
const TEST_RUNNER_PORT = '3002';

const ws  = require( 'ws' );
const os  = require( 'os' );
const fs  = require( 'fs' );
const mac = require( 'getmac' );

const executeCommand = require( './executeCommand' );
const receiveFolder  = require( './receiveFolder'  );

( async () => {
    await new Promise( ( resolve ) => setTimeout(  resolve(), 5000 ) );

    while( true ) {
        try {
            var connection = new ws( `ws://${TEST_RUNNER_IP}:${TEST_RUNNER_PORT}/nodeConnect` );

            var open = false;

            connection.once( 'open', () => {

                open = true;

                console.log( `Connected to ${TEST_RUNNER_IP}:${TEST_RUNNER_PORT}` );

                mac.getMac( ( err, macAddress ) => {
                    if( err )
                        throw err ;
                    connection.send( JSON.stringify( {
                        event   : 'NODE_INIT',
                        mac     : macAddress,
                    } ) );
                } );
                
            } );

            console.log( `Connecting to ${TEST_RUNNER_IP}:${TEST_RUNNER_PORT}` );

            connection.on( 'message', async ( msg ) => {
                var data;
                var binary;

                if( msg instanceof Buffer ) {
                    data   = JSON.parse( msg.slice( 6, Number( msg.slice( 0, 6 ).toString() ) + 6 ).toString() );
                    binary = msg.slice( Number( msg.slice( 0, 6 ).toString() ) + 6 ).toString();
                } else {
                    data = JSON.parse( msg );
                }

                if( data.event != null )
                    console.log( `Received event message: ${data.event}` );

                if( data.event == 'EXECUTE_COMMAND' ) {
                    var result = await executeCommand( data.executable, data.arguments, data.cwd );
                    if( data.msgId != null ) {
                        connection.send( JSON.stringify( {
                            ...result,
                            event: 'EXECUTE_COMMAND',
                            msgId: data.msgId,
                        } ) );
                    }
                } else if( data.event == 'TRANSMIT_FILE' ) {
                    var file = binary.toString();
                    fs.writeFile( data.destPath, file, { encoding: 'hex' }, ( err ) => console.log( err ) );
                } else if( data.event == 'TRANSMIT_FOLDER' ) {
                    var file = Buffer.from( binary, 'hex' );
                    var err  = null;
                    try {
                        await receiveFolder( data.destPath, file );
                    } catch( e ) {
                        console.log( 'RECEIVE FOLDER FAILED', e );
                        err = e;
                    }
                    if( data.msgId != null ) {
                        connection.send( JSON.stringify( {
                            err  : err,
                            event: 'TRANSMIT_FOLDER',
                            msgId: data.msgId,
                        } ) );
                    }
                }
            } );

            connection.on( 'error', ( err ) => {
                console.log( 'WebSocket error', err );
            } );
        } catch( e ) {
            console.log( 'Error while connecting', e );
        }

        await new Promise( ( resolve ) => setTimeout( () => {
            if( !open ) {
                console.log( 'Unable to connect. Retrying...' );
                resolve();
            }
        }, 10000 ) );
    }

} )();


