#!/bin/bash
mkdir /opt/testnode
cp -R * /opt/testnode
cd /opt/testnode
NODE_ENV=production npm install

cp testnode.service /lib/systemd/system/testnode.service
chmod 644 /lib/systemd/system/testnode.service
systemctl enable testnode