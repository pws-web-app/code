export default class Matrix
{

	private elements: number[][];

	private readonly rows   : number;
	private readonly columns: number;



	constructor( rows: number, columns: number, defaultValue: number = 0 )
	{
		if( rows < 1 || columns < 1 )
			throw new Error( `Cannot create matrix with ${rows} rows and ${columns} columns. Minimum matrix dimension needs to be 1x1.` );

		this.rows    = rows   ;
		this.columns = columns;

		this.setAllElementsTo( defaultValue );
	}

	public static newSquare( size: number, defaultValue: number = 0 ): Matrix
	{
		return new Matrix( size, size, defaultValue );
	}

	public static newIdentity( size: number                         ): Matrix { return Matrix.newDiagonal( size, 1 ); }
	public static newDiagonal( size: number, diagonalValues: number ): Matrix
	{
		var n = Matrix.newSquare( size );

		for( var i = 0; i < size; ++i )
			n.setElement( i, i, diagonalValues );

		return n;
	}

	public static newUpperTriangular( size: number, triangleValues: number                             ): Matrix { return this.newLowerTriangular( size, triangleValues, true ); }
	public static newLowerTriangular( size: number, triangleValues: number, transpose: boolean = false ): Matrix
	{
		var n = Matrix.newSquare( size );

		for( var i = 0; i < size; ++i )
			for( var j = 0; j < size; ++j )
				if( ( i >= j && !transpose ) || ( i <= j && transpose ) )
					n.setElement( i, j, triangleValues );

		return n;
	}



	public getRows   (): number { return this.rows   ; }
	public getColumns(): number { return this.columns; }

	public getElement( row: number, column: number ): number { return this.elements[ row ][ column ]; }

	public          setElement( row: number, column: number, to   : number ): Matrix { this.elements[ row ][ column ]  = to   ; return this; }
	public        addToElement( row: number, column: number, value: number ): Matrix { this.elements[ row ][ column ] += value; return this; }
	public subtractFromElement( row: number, column: number, value: number ): Matrix { this.elements[ row ][ column ] -= value; return this; }
	public     multiplyElement( row: number, column: number, by   : number ): Matrix { this.elements[ row ][ column ] *= by   ; return this; }
	public       divideElement( row: number, column: number, by   : number ): Matrix { this.elements[ row ][ column ] /= by   ; return this; }



	public makeCopy(): Matrix
	{
		var n = new Matrix( this.rows, this.columns );

		for( var i = 0; i < this.rows; ++i )
			for( var j = 0; j < this.columns; ++j )
				n.setElement( i, j, this.elements[ i ][ j ] );

		return n;
	}

	public clear           (               ): Matrix { return this.setAllElementsTo( 0 ); }
	public setAllElementsTo( value: number ): Matrix
	{
		this.elements = [];
		for( var i = 0; i < this.rows; ++i )
		{
			this.elements[ i ] = [];
			for( var j = 0; j < this.columns; ++j )
				this.elements[ i ][ j ] = value;
		}

		return this;
	}

	public static subtract( m1: Matrix, m2: Matrix                           ): Matrix { return m1.makeCopy().add( m2, true ); }
	public static add     ( m1: Matrix, m2: Matrix                           ): Matrix { return m1.makeCopy().add( m2       ); }
	public        subtract( m : Matrix                                       ): Matrix { return this         .add( m , true ); }
	public        add     ( m : Matrix,            negative: boolean = false ): Matrix
	{
		if( this.rows != m.getRows() || this.columns != m.getColumns() )
			throw new Error( `${m.signature()} cannot be added to ${this.signature()}. Dimensions need to be equal.` )

		for( var i = 0; i < this.rows; ++i )
			for( var j = 0; j < this.columns; ++j )
				this.elements[ i ][ j ] += m.getElement( i, j ) * ( negative ? -1 : 1 );

		return this;
	}

	public static scaleInverse( m: Matrix, value: number ): Matrix { return m.makeCopy().scale( 1 / value ); }
	public static scale       ( m: Matrix, value: number ): Matrix { return m.makeCopy().scale(     value ); }
	public        scaleInverse(            value: number ): Matrix { return this        .scale( 1 / value ); }
	public        scale       (            value: number ): Matrix
	{
		for( var i = 0; i < this.rows; ++i )
			for( var j = 0; j < this.columns; ++j )
				this.elements[ i ][ j ] *= value;

		return this;
	}

	public static iterate( m: Matrix, times: number ): Matrix { return m.makeCopy().iterate( times ); }
	public        iterate(            times: number ): Matrix
	{
		if( this.rows != this.columns )
			throw new Error( `Cannot iterate ${this.signature()}. Not an (NxN) matrix.` );

		for( var i = 0; i < times; ++i )
			this.multiply( this );

		return this;
	}



	public static multiply( m1: Matrix, m2: Matrix ): Matrix { return m1.multiply( m2 ); }
	public        multiply( m : Matrix             ): Matrix
	{
		if( this.columns != m.getRows() )
			throw new Error( `Cannot multiply ${this.signature()} and ${m.signature()}. Matrix multiplication is only possible when the column count of the left matrix matches the row count of the right matrix.` );

		var n = new Matrix( this.rows, m.columns );

		for( var i = 0; i < this.rows; ++i )
			for( var j = 0; j < m.getColumns(); ++j )
				for( var k = 0; k < this.columns; ++k )
					n.addToElement( i, j, this.elements[ i ][ k ] * m.getElement( k, j ) );

		return n;
	}

	public static transpose( m: Matrix ): Matrix { return m.transpose(); }
	public        transpose(           ): Matrix
	{
		var n = new Matrix( this.columns, this.rows );

		for( var i = 0; i < this.rows; ++i )
			for( var j = 0; j < this.columns; ++j )
				n[ j ][ i ] = this.elements[ i ][ j ];

		return n;
	}

	public signature(): string
	{
		return `Matrix (${this.rows}x${this.columns})`;
	}
	public toString(): string
	{
		var value = `${this.signature()}: {\n`;

		for( var i = 0; i < this.rows; ++i )
		{
			value += `\t{`;

			var firstItem = true;

			for( var j = 0; j < this.columns; ++j )
			{
				if( firstItem )
					firstItem = false;
				else
					value += `, `;

				value += `${this.elements[ i ][ j ]}`;
			}

			value += `}`;
			
			if( i != this.rows - 1 )
				value += `,`

			value += `\n`;
		}

		value += `}`;

		return value;
	}

}
