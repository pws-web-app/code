import express from 'express';
import path from 'path';
import index from './routes/index';

( async () => {

    let app = express();

    app.set( 'views', path.join( __dirname, '../views' ) );
    app.set( 'view engine', 'ejs' );

    app.use( express.static( path.join( __dirname, '../public' ) ) );

    app.use( '/', index );

    app.listen( 3003, () => console.log( '[HTTP SERVER] Started on port 3003' ) );

    setTimeout( () => require( './loader' ), 500 );

} )();