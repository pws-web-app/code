import DataBlock, { IDataBlock } from "./data_manager";
import * as fs from 'fs';
import * as vega from 'vega';
import express from 'express';

export enum GraphType
{
	StackedBar  = "stackedBar" ,
	GroupedBar  = "groupedBar" ,

	Line        = "line"       ,
	StackedArea = "stackedArea",

	Pie         = "pie"        ,

	Scatter     = "scatter"    ,
	Regression  = "regression" ,

	Heatmap     = "heatmap"
}

export default class Graph
{

	public static readonly graphPropsPath = "graphs";

	public static defaultProps = {
		data    : []                                          ,

		$schema : "https://vega.github.io/schema/vega/v5.json",
		width   : 1000                                        ,
		height  : 500                                         ,
		padding : 5                                           ,
		autosize: "pad"
	};

	public static async loadGraph( db: IDataBlock ): Promise<vega.Spec>
	{
		var graph: any = { ...this.defaultProps, data: [] };

		graph.data.push( db.dataToVega() );

		var graphProps = await new Promise( ( resolve, reject ) => {
			fs.readFile( this.graphPropsPath + "/" + db.GraphType + ".json", ( error, contents ) => {
				if( error )
					return reject( error );

				resolve( JSON.parse( contents.toString() ) );
			} );
		} )

		graph = { ...graph, ...graphProps };

		return graph;
	}

	public static renderSVG( graph: vega.Spec ): Promise<string>
	{
		return new Promise( ( resolve ) => {
			var view = new vega.View( vega.parse( graph ), { renderer: 'none' } );
	
			view.toSVG().then(function( svg ) {
				resolve( svg );
			} ).catch( function( err ) { console.error( err ); } );
		} );
	}

	private static editorSpecs: { [ key: string ]: vega.Spec } = {};

	public static getEditorPath( graph: vega.Spec ): string {
		function makeID(length) {
			var result           = '';
			var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
			var charactersLength = characters.length;
			for ( var i = 0; i < length; i++ ) {
			   result += characters.charAt(Math.floor(Math.random() * charactersLength));
			}
			return result;
		}

		let id = makeID( 16 );
		Graph.editorSpecs[ id ] = graph;

		return '/editor/#/examples/vega/' + id;
	}

	public static getEditorSpec( id: string ) {
		return Graph.editorSpecs[ id ];
	}

}
