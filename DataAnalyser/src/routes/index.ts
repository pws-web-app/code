import express from 'express';
import { DB_NumberList, DB_NumberArray, DB_Matrix } from '../data_manager';
import Graph from '../graph';
import Matrix from '../matrix';

let router = express.Router();

router.get( '/', async function ( req, res ) {
    /*var db = new DB_NumberList( {
        t1: 1,
        t2: 2,
        t3: 3
    } );*/
	/*var db = new DB_NumberArray( {
		t1: { y1: 5, y2: 6 },
		t2: { y1: 8, y2: 7 },
		t3: { y1: 4, y2: 2 }
	}, [ 'y1', 'y2' ], false )*/
	var m = Matrix.newIdentity( 4 );
	enum Test { T1, A2, T3, T4 };
	var db = new DB_Matrix( DB_Matrix.matrixToRawData( m ), Object.values( Test ) as any, ["lol", "five", "six", "seven"] );

    res.redirect( Graph.getEditorPath( await Graph.loadGraph( db ) ) );
} );

router.get( '/editor/spec/vega/:specId.vg.json', async function( req, res ) {
    res.send( JSON.stringify( Graph.getEditorSpec( req.params.specId ), null, 2 ) );
} );
export default router;
