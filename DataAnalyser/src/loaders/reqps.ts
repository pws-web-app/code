import { Loader, Result } from "../loader";
import * as vega from 'vega';
import { DB_NumberArray } from '../data_manager';
import Graph from '../graph';

export default class CPUAverage implements Loader {
    
    resultFilters: Loader[ 'resultFilters' ] = {
        minCount: 1,
        maxCount: 0,
    }

    async process( dataArray: Result[] ): Promise<vega.Spec> {

        var graphData = {};


        dataArray.forEach( ( data ) => {
            graphData[ data._metaData.setup ] = data.data.summary.requests / ( data.data.summary.duration / Math.pow( 10, 6 ) );
        } );

        var db = new DB_NumberArray( graphData );
    
        return await Graph.loadGraph( db );
    }

}