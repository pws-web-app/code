import { Loader, Result } from "../loader";
import * as vega from 'vega';
import { DB_NumberArray } from '../data_manager';
import Graph from '../graph';
import { Terminal } from "terminal-kit";

export default class CPUUsage implements Loader {
    
    resultFilters: Loader[ 'resultFilters' ] = {
        minCount: 1,
        maxCount: 1
    }

    vms = [];
    resource = null;

    /**
     * Can be used to ask additional questions to the user
     * 
     * @param term Instance of terminal-kit to control terminal
     */
    init( term: Terminal, dataArray: Result[] ): Promise<void> {
        return new Promise( async ( resolve ) => {

            var resources = [
                'cpu',
                'memory',
                'diskRead',
                'diskWrite',
                'networkIn',
                'networkOut',
            ];

            term.cyan( 'Please choose a resource to show in the graph:\n' );

            await new Promise( ( resolve, reject ) => term.gridMenu( [ '[Exit]', ...resources ], ( err, response ) => {
                if ( err )
                    reject( err );
                else if ( response.selectedIndex == 0 )
                    process.exit();
                else {
                    this.resource = resources[ response.selectedIndex - 1 ];
                }

                resolve();
            } ) );
            
            term.clear();

            var data = dataArray[ 0 ];

            var vms = Object.keys( data._metaData.resources );

            var names = vms.map( ( vm ) => `${vm} ${data._metaData.resources[ vm ].hostname}`)

            var selected = [];
            var r = false;

            while( !r ) {
                selected.sort();

                await new Promise( ( resolve, reject ) => {
                    term.clear();

                    selected.forEach( ( vm ) => term.white( ` - ${names[ vms.indexOf( vm ) ]}\n` ) );
                    
                    term.cyan( 'Please choose the VMs to show in the graph:\n' );

                    term.gridMenu( [ '[Exit]', '[Continue]', ...names ], ( err, response ) => {
                        if ( err )
                            reject( err );
                        else if ( response.selectedIndex == 0 )
                            process.exit();
                        else if( response.selectedIndex == 1 )
                            r = true;
                        else {
                            var i = response.selectedIndex - 2;
                            if( selected.includes( vms[ i ] ) )
                                selected = selected.filter( ( vm ) => vm != vms[ i ] );
                            else
                                selected.push( vms[ i ] );
                        }

                        resolve();
                    } );
                } );
                
            }
            
            term.clear();
            this.vms = selected;

            resolve();
        } );
    }

    async process( dataArray: Result[] ): Promise<vega.Spec> {
        var data = dataArray[ 0 ];

        var startTime = new Date( data._metaData.startTime );

        var graphData = {};

        this.vms.forEach( ( vm ) => {
            data._metaData.resources[ vm ].readings.forEach( ( r ) => {
                if ( graphData[ ( new Date( r.time ).getTime() - startTime.getTime() ) / 1000 ] == null )
                    graphData[ ( new Date( r.time ).getTime() - startTime.getTime() ) / 1000 ] = {};
                graphData[ ( new Date( r.time ).getTime() - startTime.getTime() ) / 1000 ][ vm ] = r[ this.resource ];
            } );
        } );

        


        var db = new DB_NumberArray( graphData, this.vms );
    
        return await Graph.loadGraph( db );
    }

}