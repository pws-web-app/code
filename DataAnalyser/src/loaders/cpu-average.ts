import { Loader, Result } from "../loader";
import * as vega from 'vega';
import { DB_NumberArray } from '../data_manager';
import Graph from '../graph';

export default class CPUAverage implements Loader {
    
    resultFilters: Loader[ 'resultFilters' ] = {
        minCount: 1,
        maxCount: 0,
    }

    async process( dataArray: Result[] ): Promise<vega.Spec> {

        function getAverage( result: Result ) {
            var totalCpu = 0;
            var cpuSum = 0;
            var readingCount = 0;
            Object.values( result._metaData.resources ).forEach( ( resources ) => {
                readingCount += resources.readings.length;
                totalCpu     += resources.available.cpu  ;

                resources.readings.forEach( ( r ) => {
                    cpuSum += r.cpu;
                } );
            } );

            return {
                total  : totalCpu,
                average: cpuSum / readingCount,
            }
        }

        var graphData = {};

        dataArray.forEach( ( data ) => {
            graphData[ data._metaData.vars.reqps ] = getAverage( data ).average;
        } );

        var db = new DB_NumberArray( graphData );
    
        return await Graph.loadGraph( db );
    }

}