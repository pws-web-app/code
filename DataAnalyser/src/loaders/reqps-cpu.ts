import { Loader, Result } from "../loader";
import * as vega from 'vega';
import { DB_NumberArray } from '../data_manager';
import Graph from '../graph';

export default class CPUAverage implements Loader {
    
    resultFilters: Loader[ 'resultFilters' ] = {
        minCount: 1,
        maxCount: 0,
    }

    async process( dataArray: Result[] ): Promise<vega.Spec> {

        function getAverage( result: Result ) {
            var data = {};

            Object.keys( result._metaData.resources ).forEach( ( vmid ) => {
                var totalCpu = 0;

                result._metaData.resources[ vmid ].readings.forEach( ( r ) => {
                    totalCpu += r.cpu;
                } );

                data[ vmid ] = totalCpu / result._metaData.resources[ vmid ].readings.length;
            } );

            return data;
        }

        var graphData = {};

        dataArray.forEach( ( data ) => {
            graphData[ data._metaData.vars.reqps ] = getAverage( data );
        } );

        var keys = [];
        Object.values( graphData ).forEach( ( o ) => {
            Object.keys( o ).forEach( ( k ) => keys.includes( k ) ? null : keys.push( k ) );
        } );

        var db = new DB_NumberArray( graphData, keys );
    
        return await Graph.loadGraph( db );
    }

}