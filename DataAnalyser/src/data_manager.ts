import * as fs from "fs";
import Matrix from "./matrix";
import { GraphType } from "./graph";

export default class DataBlock
{

	protected data: object;

	public get Data()
	{
		return this.data;
	}

	constructor( data: object )
	{
		this.data = data;
	}

}

export interface IDataBlock
{
	         dataToVega(): object   ;
	readonly GraphType   : GraphType;
}



export class DB_Matrix extends DataBlock implements IDataBlock
{

	private readonly rowLabels   : string[];
	private readonly columnLabels: string[];

	constructor( rawData: object, rowLabels?: string[], columnLabels?: string[] )
	{
		super( rawData );

		this.rowLabels    = rowLabels   ;
		this.columnLabels = columnLabels;
	}

	public static matrixToRawData( data: Matrix ): object
	{
		var rawData = {};

		rawData[ "rows"    ] = data.getRows   ();
		rawData[ "columns" ] = data.getColumns();

		for( var i = 0; i < data.getRows(); ++i )
		{
			rawData[ i ] = {};
			for( var j = 0; j < data.getColumns(); ++j )
				rawData[ i ][ j ] = data.getElement( i, j );
		}

		return rawData;
	}

	public dataToVega(): object
	{
		var json = {
			name: "table",
			values: []
		};

		for( var x = 0; x < this.data[ "columns" ]; ++x )
			for( var y = 0; y < this.data[ "rows" ]; ++y )
				json.values.push( {
					x    : this.columnLabels == null ? x : this.columnLabels[ x ],
					y    : this.rowLabels    == null ? y : this.rowLabels   [ y ],
					value: this.data[ x ][ y ]
				} );

		return json;
	}

	public readonly GraphType = GraphType.Heatmap;

}

/*
 * 'keys' == null:
 * {
 *     label1: value1,
 *     label2: value2,
 *     ...
 * }
 *
 * 'keys' != null:
 * {
 *     x1: { 'keys[ 0 ]': height1_1, 'keys[ 1 ]': height1_2, ... },
 *     x2: { 'keys[ 0 ]': height2_1, 'keys[ 1 ]': height2_2, ... },
 *     ...
 * }
 */
export class DB_NumberList extends DataBlock implements IDataBlock
{

	private readonly keys: Array<string>;

	constructor( data: object, keys?: Array<string> )
	{
		super( data );

		this.keys = keys;
	}

	public dataToVega(): object
	{
		var json = {
			name: "table",
			values: [],
			transform: [
			  {
				type   : "stack"       ,
				groupby: [ "x" ]       ,
				sort   : { field: "c" },
				field  : "y"
			  }
			]
		};

		if( this.keys == null )
			for( var key in this.data )
					json.values.push( {
						x: key             ,
						y: this.data[ key ],
						c: 0
					} );
		else
			for( var key in this.data )
				for( var i = 0; i < this.keys.length; ++i )
					json.values.push( {
						x: key                               ,
						y: this.data[ key ][ this.keys[ i ] ],
						c: i
					} );

		return json;
	}

	public readonly GraphType = GraphType.StackedBar;

}

/*
 * 'keys' != null:
 * {
 *     x1: { 'keys[ 0 ]': y1_1, 'keys[ 1 ]': y1_2, ... },
 *     x2: { 'keys[ 0 ]': y2_1, 'keys[ 1 ]': y2_2, ... },
 *     ...
 * }
 *
 * 'keys' == null:
 * {
 *     x1: y1,
 *     x2: y2,
 *     ...
 * }
 */
export class DB_NumberArray extends DataBlock implements IDataBlock
{

	private readonly keys: Array<string>;
	private readonly area: boolean      ;

	constructor( data: object, keys?: Array<string>, area: boolean = false )
	{
		super( data );

		this.keys = keys;
		this.area = area;
	}

	public dataToVega(): object
	{
		var json = {
			name: "table",
			values: [],
			transform: [
			  {
				type   : "stack"       ,
				groupby: [ "x" ]       ,
				sort   : { field: "c" },
				field  : "y"
			  }
			]
		};

		if( this.keys == null )
			for( var key in this.data ) {
				var x = isNaN( key as any as number ) ? key : Number( key );
				json.values.push( {
					x: x            ,
					y: this.data[ key ],
					c: 0
				} );
			}
		else
			for( var key in this.data )
				for( var i = 0; i < this.keys.length; ++i ) {
					if( this.data[ key ][ this.keys[ i ] ] == null )
						continue;
					var x = isNaN( key as any as number ) ? key : Number( key );
					json.values.push( {
						x: x                               ,
						y: this.data[ key ][ this.keys[ i ] ],
						c: this.keys[ i ]
					} );
				}

		json.values = json.values.sort( ( a, b ) => {
			if( a.x < b.x )
				return -1;
			else if( a.x > b.x )
				return 1;
			return 0;
		} );

		return json;
	}
	
	public get GraphType()
	{
		return this.area ? GraphType.StackedArea : GraphType.Line;
	};

}



export class DataManager
{

	public static saveDataBlock( db: DataBlock, path: string ): Promise<void>
	{
		var json = JSON.stringify( db.Data );

		return new Promise( ( resolve, reject ) => {
			fs.writeFile( path, json, ( error ) => {
				if( error )
					return reject( error );

				resolve();
			} );
		} );
	}

	public static loadDataBlock( path: string ): Promise<DataBlock>
	{
		return new Promise( ( resolve, reject ) => {
			fs.readFile( path, ( error, contents ) => {
				if( error )
					return reject( error );

				var db = new DataBlock( JSON.parse( contents.toString() ) );

				resolve( db );
			} );
		} )
	}

}
