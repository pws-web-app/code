import { terminal as term, Terminal, terminal } from 'terminal-kit';
import fs from 'fs';
import path from 'path';
import * as vega from 'vega';
import Graph from './graph';

export interface Result {
    _metaData: {
        setup: string;
        test: string;
        vars: {
            [ key: string ]: string
        },
        /**
         * ISO Format (e.g. 2019-11-04T07:30:17.378Z)
         */
        startTime: string;
        /**
         * ISO Format (e.g. 2019-11-04T07:30:17.378Z)
         */
        endTime: string;

        resources: {
            [ containerId: string ]: {
                hostname: string;
                available: {
                    cpu: number;
                    memory: number;
                },
                readings: [
                    {
                        /**
                         * ISO Format (e.g. 2019-11-04T07:30:17.378Z)
                         */
                        time: string;
                        cpu: number;
                        memory: number;
                        diskRead: number;
                        diskWrite: number;
                        networkIn: number;
                        networkOut: number;
                    }
                ]
            }
        }
    },
    data: any;
}

export declare class Loader {

    resultFilters: {
        /**
         * Minimum required result files that should be specified.
         * @default 1
         */
        minCount?: Number,
        /**
         * Maximum required result files that should be specified.
         * 0 = infinite.
         * @default 0
         */
        maxCount?: Number,

        /**
         * Required setup or setups.
         * @default null
         */
        setup?: Array<string>|string,

        /**
         * Required test or tests.
         * @default null
         */
        test?: Array<string>|string,
    }

    /**
     * Can be used to ask additional questions to the user
     * 
     * @param term Instance of terminal-kit to control terminal
     * @param files Array of file paths that have been selected
     */
    init?( term: Terminal, data: Result[] ): Promise<void>|void;

    /**
     * Processes the result files to a vega graph.
     * 
     * @param data Selected Files in Object form
     */
    process( data: Result[] ): Promise<vega.Spec>|vega.Spec;

}

class LoaderController {

    private static getLoaders(): Promise<string[]> {
        return new Promise( ( resolve, reject ) => {
            fs.readdir( path.join( __dirname, './loaders' ), ( err, files ) => {
                if( err )
                    return reject( err );
                resolve( files );
            } );
        } );
    }

    private static async getResults( filterSetup?: string|Array<string>, filterTest?: string|Array<string> ): Promise<{ fileName: string, isDirectory: boolean, subFiles: void|Array<string> }[]> {
        var results: { fileName: string, isDirectory: boolean, subFiles: void|Array<string> }[] = await new Promise( ( resolve, reject ) => {
            fs.readdir( path.join( __dirname, '../results' ), async ( err, files ) => {
                if( err )
                    return reject( err );

                var fileInfo: { fileName: string, isDirectory: boolean, subFiles: void|Array<string> }[] = await Promise.all( files.map( ( f ) => new Promise( ( resolve, reject ) => {
                    fs.stat( path.join( __dirname, '../results', f ), ( err, stats ) => {
                        if( err )
                            return reject( err );

                        resolve( {
                            fileName   : f,
                            isDirectory: stats.isDirectory(),
                            subFiles   : null,
                        } );
                    } );
                } ) ) );

                resolve( fileInfo );
            } );
        } );

        async function filterResults( results: { fileName: string; isDirectory: boolean; subFiles: void|Array<string> }[] ) {
            function filterFiles( files ) {
                var filtered = [ ...files ];
                if( filterSetup != null ) {
                    filtered = filtered.filter( ( f ) => {
                        try {
                            var URI = LoaderController.parseURI( path.parse( f.fileName ).name.split( '|' )[ 1 ] );
                        } catch( e ) {
                            return false;
                        }
                        
                        if( Array.isArray( filterSetup ) )
                            return filterSetup.includes( URI.setup );
                        else
                            return filterSetup == URI.setup;
                    } );
                } 
                if( filterTest != null ) {
                    filtered = filtered.filter( ( f ) => {
                        try {
                            var URI = LoaderController.parseURI( path.parse( f.fileName ).name.split( '|' )[ 1 ] );
                        } catch( e ) {
                            return false;
                        }

                        if( Array.isArray( filterTest ) )
                            return filterTest.includes( URI.test );
                        else
                            return filterTest == URI.test;
                    } );
                }

                return filtered;
            }
            
            var files = filterFiles( results.filter( ( r ) => !r.isDirectory ) );

            var dirs = [];
            
            await Promise.all( results.filter( ( r ) => r.isDirectory ).map( ( r ) => new Promise( ( resolve, reject ) => {
                fs.readdir( path.resolve( __dirname, '../results', r.fileName ), ( err, files ) => {
                    if( err )
                        return reject( err );
                    var filtered = filterFiles( files.map( ( f ) => ( { fileName: f, isDirectory: false, subFiles: null } ) ) );
                    if( filtered.length > 0 )
                        dirs.push( { ...r, subFiles: filtered.map( ( f ) => f.fileName ) } );
                    resolve();
                } );
            } ) ) );

            return [ ...dirs, ...files ];
        }

        return await filterResults( results );
    }

    private static selectLoader(): Promise<string> {
        return new Promise( async ( resolve, reject ) => {
            var loaders = await this.getLoaders();
    
            var loaderNames = loaders.map( ( l ) => path.parse( l ).name );

            term.clear();

            term.cyan( 'Choose a loader to use:\n' );

            term.gridMenu( [ '[Exit]', ...loaderNames ], ( err, response ) => {
                if( err )
                    reject( err );
                else if( response.selectedIndex == 0 )
                    process.exit();
                else
                    resolve( loaders[ response.selectedIndex - 1 ] );
            } );
        } );
    }

    private static selectResults( resultFilters: Loader[ 'resultFilters' ] ): Promise<{ fileName: string, isDirectory: boolean; subFiles: void|Array<string> }[]> {
        return new Promise( ( resolve, reject ) => {

            var files: { fileName: string, isDirectory: boolean; subFiles: void|Array<string> }[] = [];
            var fileCount = 0;

            function getFileCount() {
                var length = files.filter( ( f ) => !f.isDirectory ).length;

                files.filter( ( f ) => f.isDirectory ).forEach( ( f ) => f.subFiles != null ? length += ( f.subFiles as string[] ).length : null );

                return length;
            }

            async function addPrompt( addSubset = false ) {
                var results = await LoaderController.getResults( resultFilters.setup, resultFilters.test );

                var fileKeys = files.filter( ( f ) => f.isDirectory == addSubset ).map( ( f ) => f.fileName );
                
                var availableResults = [ { fileName: '[Go Back]', isDirectory: false, subFiles: null }, ...results.filter( ( r ) => r.isDirectory == addSubset && !fileKeys.includes( r.fileName ) ) ];
                
                term.clear();

                term.cyan( `Please choose a ${addSubset ? 'subset' : 'file'}:\n` );

                term.gridMenu( availableResults.map( ( r ) => path.parse( r.fileName ).name ), ( err, response ) => {
                    if( err )
                        throw err;
                    if( response.selectedIndex > 0 )
                        files.push( availableResults[ response.selectedIndex ] );
                    mainMenu();
                } );
            }

            async function addAll() {
                files = await LoaderController.getResults( resultFilters.setup, resultFilters.test );
                mainMenu();
            }

            function removePrompt() {
                term.clear();

                term.cyan( `Please choose an item to remove:\n` );

                term.gridMenu( [ '[Go Back]', ...files.map( ( r ) => path.parse( r.fileName ).name ) ], ( err, response ) => {
                    if( err )
                        throw err;
                    if( response.selectedIndex > 0 )
                        files.splice( response.selectedIndex - 1, 1 );
                    mainMenu();
                } );
            }

            function removeAll() {
                files = [];
                mainMenu();
            }

            function exitPrompt() {
                term.clear();

                term.cyan( 'Are you sure you want to exit?\n' );

                term.singleColumnMenu( [ 'Yes ', 'No ' ], ( err, response ) => {
                    if( err )
                        throw err;
                    if( response.selectedIndex == 0 )
                        process.exit( 0 );
                    else
                        mainMenu();
                } );
            }

            function continuePrompt() {
                term.clear();

                if( fileCount < resultFilters.minCount ) {
                    term.red( `${fileCount} item${fileCount != 1 ? 's' : ''} have been selected, but at least ${resultFilters.minCount} are needed.\n` );
                } else if( fileCount > resultFilters.maxCount && resultFilters.maxCount != 0 ) {
                    term.red( `${fileCount} item${fileCount != 1 ? 's' : ''} have been selected, but a maximum of ${resultFilters.maxCount} is allowed.\n` );
                } else {
                    return resolve( files );
                }

                term.gridMenu( [ 'Go Back' ], ( err, response ) => {
                    if( err )
                        throw err;
                    mainMenu();
                } );
            }

            async function mainMenu() {
                function mainSelect() {
                    return new Promise( ( resolve, reject ) => {
                        term.clear();
    
                        if( files.length == 0 ) {
                            term.cyan( 'Please select the result files to use for this loader.\n' );
                            term.white( '0 files selected\n' );
                        } else {
                            term.cyan( `${fileCount} item${fileCount != 1 ? 's' : ''} selected:\n` );
                            files.forEach( ( f ) => {
                                if( f.isDirectory )
                                    term.white( ` - [SUBSET] ${f.fileName}\n` );
                                else
                                    term.white( ` - [FILE  ] ${f.fileName}\n` );
                            } );
                            term.white( '\n' );
                        }

                        term.green( ` - A minimum of ${resultFilters.minCount} items are required.\n` );
                        if( resultFilters.maxCount != 0 )
                            term.green( ` - A maximum of ${resultFilters.maxCount} items are allowed.\n` );
                        if( resultFilters.setup != null )
                            term.green( ` - Results must have "${resultFilters.setup}" as setup.\n` );
                        if( resultFilters.test != null )
                            term.green( ` - Results must have "${resultFilters.test}" as test.\n` );
    
                        term.gridMenu( [
                            'Continue'  ,
                            'Add File'  ,
                            'Add Subset',
                            'Add All'   ,
                            'Remove Item',
                            'Remove All' ,
                            'Exit',
                        ], ( err, response ) => {
                            if( err )
                                reject( err );
                            else
                                resolve( response.selectedText );
                        } );
                    } );
                }

                fileCount = getFileCount();

                var action = await mainSelect();

                switch( action ) {
                    case 'Continue': {
                        continuePrompt();
                        break;
                    }
                    case 'Add File': {
                        addPrompt( false );
                        break;
                    }
                    case 'Add Subset': {
                        addPrompt( true );
                        break;
                    }
                    case 'Add All': {
                        addAll();
                        break;
                    }
                    case 'Remove Item': {
                        removePrompt();
                        break;
                    }
                    case 'Remove All': {
                        removeAll();
                        break;
                    }
                    case 'Exit': {
                        exitPrompt();
                        break;
                    }
                }
            }

            mainMenu();
        } );
    }

    private static parseURI( URI ) {
        var data = {
            setup    : null,
            test     : null,
            subtest  : null,
            variables: {}  ,
            uri      : URI ,
        };

        var parts = URI.split( ':' );
        if( parts.length != 2 )
            throw new Error( `Invalid URI: ${URI}.` );
        
        data.setup = parts[ 0 ];
        
        var parts2 = parts[ 1 ].split( '/' );
        if( parts2.length == 2 ) {
            data.test = parts2[ 0 ];

            var parts3 = parts2[ 1 ].split( '?' );
            if( parts3.length == 2 ) {
                data.subtest = parts3[ 0 ];

                var parts4 = parts3[ 1 ].split( '&' );
                parts4.forEach( ( item ) => {
                    var parts5 = item.split( '=' );
                    if( parts5.length == 2 ) {
                        data.variables[ parts5[ 0 ] ] = parts5[ 1 ];
                    } else {
                        throw new Error( `Invalid variable part [ ${item} ] in URI: ${URI}.` );
                    }
                } );
            } else if( parts3.length == 1 ) {
                data.subtest = parts3[ 0 ];
            }
        } else if( parts2.length == 1 ) {

            var parts3 = parts2[ 0 ].split( '?' );
            if( parts3.length == 2 ) {
                data.test = parts3[ 0 ];

                var parts4 = parts3[ 1 ].split( '&' );
                parts4.forEach( ( item ) => {
                    var parts5 = item.split( '=' );
                    if( parts5.length == 2 ) {
                        data.variables[ parts5[ 0 ] ] = parts5[ 1 ];
                    } else {
                        throw new Error( `Invalid variable part [ ${item} ] in URI: ${URI}.` );
                    }
                } );
            } else if( parts3.length == 1 ) {
                data.test = parts3[ 0 ];
            }
        }

        return data;
    }

    static async init() {
        var loaderName = await this.selectLoader();
        var loader: Loader = new ( require( path.join( __dirname, './loaders', loaderName ) ).default );

        var files = await this.selectResults( {
            minCount: 1,
            maxCount: 0,
            setup   : null,
            test    : null,
            ...loader.resultFilters
        } );
        var filePaths = [];
        files.forEach( ( f ) => {
            if( !f.isDirectory )
                return filePaths.push( path.join( __dirname, '../results', f.fileName ) );
            else {
                ( f.subFiles as string[] ).forEach( ( s ) => {
                    filePaths.push( path.join( __dirname, '../results', f.fileName, s ) );
                } );
            }
        } );

        term.clear();

        term.clear();

        var data = filePaths.map( ( f ) => require( f ) );


        if ( loader.init != null )
            await loader.init( term, data );

        var spec = await loader.process( data );

        term.cyan( `Graph ready:\n` );
        term.white( `http://localhost:3003${Graph.getEditorPath( spec )}` );
        term.cyan( '\n' );

        term.singleColumnMenu( [ 'Next Graph ', 'Exit' ], ( err, response ) => {
            if( err )
                throw err;
            if( response.selectedIndex == 1 )
                process.exit( 0 );
            else
                LoaderController.init();
        } );
    }

}

LoaderController.init();

/*term.gridMenu( items, {}, function( error , response ) {
			
    if ( error )
    {
        term.red.bold( "\nAn error occurs: " + error + "\n" ) ;
        term.processExit( 5 ) ;
        return ;
    }
    
    term.green( "\n#%s %s: %s (%s,%s)\n" ,
        response.selectedIndex ,
        response.selectedText ,
        response.x ,
        response.y
    ) ;
    
    term.processExit( 5 );
} ) ;*/