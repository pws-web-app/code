(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Matrix = (function () {
        function Matrix(rows, columns, defaultValue) {
            if (defaultValue === void 0) { defaultValue = 0; }
            if (rows < 1 || columns < 1)
                throw new Error("Cannot create matrix with " + rows + " rows and " + columns + " columns. Minimum matrix dimension needs to be 1x1.");
            this.rows = rows;
            this.columns = columns;
            this.setAllElementsTo(defaultValue);
        }
        Matrix.newSquare = function (size, defaultValue) {
            if (defaultValue === void 0) { defaultValue = 0; }
            return new Matrix(size, size, defaultValue);
        };
        Matrix.newIdentity = function (size) { return Matrix.newDiagonal(size, 1); };
        Matrix.newDiagonal = function (size, diagonalValues) {
            var n = Matrix.newSquare(size);
            for (var i = 0; i < size; ++i)
                n.setElement(i, i, diagonalValues);
            return n;
        };
        Matrix.newUpperTriangular = function (size, triangleValues) { return this.newLowerTriangular(size, triangleValues, true); };
        Matrix.newLowerTriangular = function (size, triangleValues, transpose) {
            if (transpose === void 0) { transpose = false; }
            var n = Matrix.newSquare(size);
            for (var i = 0; i < size; ++i)
                for (var j = 0; j < size; ++j)
                    if ((i >= j && !transpose) || (i <= j && transpose))
                        n.setElement(i, j, triangleValues);
            return n;
        };
        Matrix.prototype.getRows = function () { return this.rows; };
        Matrix.prototype.getColumns = function () { return this.columns; };
        Matrix.prototype.getElement = function (row, column) { return this.elements[row][column]; };
        Matrix.prototype.setElement = function (row, column, to) { this.elements[row][column] = to; return this; };
        Matrix.prototype.addToElement = function (row, column, value) { this.elements[row][column] += value; return this; };
        Matrix.prototype.subtractFromElement = function (row, column, value) { this.elements[row][column] -= value; return this; };
        Matrix.prototype.multiplyElement = function (row, column, by) { this.elements[row][column] *= by; return this; };
        Matrix.prototype.divideElement = function (row, column, by) { this.elements[row][column] /= by; return this; };
        Matrix.prototype.makeCopy = function () {
            var n = new Matrix(this.rows, this.columns);
            for (var i = 0; i < this.rows; ++i)
                for (var j = 0; j < this.columns; ++j)
                    n.setElement(i, j, this.elements[i][j]);
            return n;
        };
        Matrix.prototype.clear = function () { return this.setAllElementsTo(0); };
        Matrix.prototype.setAllElementsTo = function (value) {
            this.elements = [];
            for (var i = 0; i < this.rows; ++i) {
                this.elements[i] = [];
                for (var j = 0; j < this.columns; ++j)
                    this.elements[i][j] = value;
            }
            return this;
        };
        Matrix.subtract = function (m1, m2) { return m1.makeCopy().add(m2, true); };
        Matrix.add = function (m1, m2) { return m1.makeCopy().add(m2); };
        Matrix.prototype.subtract = function (m) { return this.add(m, true); };
        Matrix.prototype.add = function (m, negative) {
            if (negative === void 0) { negative = false; }
            if (this.rows != m.getRows() || this.columns != m.getColumns())
                throw new Error(m.signature() + " cannot be added to " + this.signature() + ". Dimensions need to be equal.");
            for (var i = 0; i < this.rows; ++i)
                for (var j = 0; j < this.columns; ++j)
                    this.elements[i][j] += m.getElement(i, j) * (negative ? -1 : 1);
            return this;
        };
        Matrix.scaleInverse = function (m, value) { return m.makeCopy().scale(1 / value); };
        Matrix.scale = function (m, value) { return m.makeCopy().scale(value); };
        Matrix.prototype.scaleInverse = function (value) { return this.scale(1 / value); };
        Matrix.prototype.scale = function (value) {
            for (var i = 0; i < this.rows; ++i)
                for (var j = 0; j < this.columns; ++j)
                    this.elements[i][j] *= value;
            return this;
        };
        Matrix.iterate = function (m, times) { return m.makeCopy().iterate(times); };
        Matrix.prototype.iterate = function (times) {
            if (this.rows != this.columns)
                throw new Error("Cannot iterate " + this.signature() + ". Not an (NxN) matrix.");
            for (var i = 0; i < times; ++i)
                this.multiply(this);
            return this;
        };
        Matrix.multiply = function (m1, m2) { return m1.multiply(m2); };
        Matrix.prototype.multiply = function (m) {
            if (this.columns != m.getRows())
                throw new Error("Cannot multiply " + this.signature() + " and " + m.signature() + ". Matrix multiplication is only possible when the column count of the left matrix matches the row count of the right matrix.");
            var n = new Matrix(this.rows, m.columns);
            for (var i = 0; i < this.rows; ++i)
                for (var j = 0; j < m.getColumns(); ++j)
                    for (var k = 0; k < this.columns; ++k)
                        n.addToElement(i, j, this.elements[i][k] * m.getElement(k, j));
            return n;
        };
        Matrix.transpose = function (m) { return m.transpose(); };
        Matrix.prototype.transpose = function () {
            var n = new Matrix(this.columns, this.rows);
            for (var i = 0; i < this.rows; ++i)
                for (var j = 0; j < this.columns; ++j)
                    n[j][i] = this.elements[i][j];
            return n;
        };
        Matrix.prototype.signature = function () {
            return "Matrix (" + this.rows + "x" + this.columns + ")";
        };
        Matrix.prototype.toString = function () {
            var value = this.signature() + ": {\n";
            for (var i = 0; i < this.rows; ++i) {
                value += "\t{";
                var firstItem = true;
                for (var j = 0; j < this.columns; ++j) {
                    if (firstItem)
                        firstItem = false;
                    else
                        value += ", ";
                    value += "" + this.elements[i][j];
                }
                value += "}";
                if (i != this.rows - 1)
                    value += ",";
                value += "\n";
            }
            value += "}";
            return value;
        };
        return Matrix;
    }());
    exports.default = Matrix;
});
