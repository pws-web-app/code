var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "terminal-kit", "fs", "path", "./graph"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var terminal_kit_1 = require("terminal-kit");
    var fs_1 = __importDefault(require("fs"));
    var path_1 = __importDefault(require("path"));
    var graph_1 = __importDefault(require("./graph"));
    var LoaderController = (function () {
        function LoaderController() {
        }
        LoaderController.getLoaders = function () {
            return new Promise(function (resolve, reject) {
                fs_1.default.readdir(path_1.default.join(__dirname, './loaders'), function (err, files) {
                    if (err)
                        return reject(err);
                    resolve(files);
                });
            });
        };
        LoaderController.getResults = function (filterSetup, filterTest) {
            return __awaiter(this, void 0, void 0, function () {
                function filterResults(results) {
                    return __awaiter(this, void 0, void 0, function () {
                        function filterFiles(files) {
                            var filtered = __spreadArrays(files);
                            if (filterSetup != null) {
                                filtered = filtered.filter(function (f) {
                                    try {
                                        var URI = LoaderController.parseURI(path_1.default.parse(f.fileName).name.split('|')[1]);
                                    }
                                    catch (e) {
                                        return false;
                                    }
                                    if (Array.isArray(filterSetup))
                                        return filterSetup.includes(URI.setup);
                                    else
                                        return filterSetup == URI.setup;
                                });
                            }
                            if (filterTest != null) {
                                filtered = filtered.filter(function (f) {
                                    try {
                                        var URI = LoaderController.parseURI(path_1.default.parse(f.fileName).name.split('|')[1]);
                                    }
                                    catch (e) {
                                        return false;
                                    }
                                    if (Array.isArray(filterTest))
                                        return filterTest.includes(URI.test);
                                    else
                                        return filterTest == URI.test;
                                });
                            }
                            return filtered;
                        }
                        var files, dirs;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    files = filterFiles(results.filter(function (r) { return !r.isDirectory; }));
                                    dirs = [];
                                    return [4, Promise.all(results.filter(function (r) { return r.isDirectory; }).map(function (r) { return new Promise(function (resolve, reject) {
                                            fs_1.default.readdir(path_1.default.resolve(__dirname, '../results', r.fileName), function (err, files) {
                                                if (err)
                                                    return reject(err);
                                                var filtered = filterFiles(files.map(function (f) { return ({ fileName: f, isDirectory: false, subFiles: null }); }));
                                                if (filtered.length > 0)
                                                    dirs.push(__assign(__assign({}, r), { subFiles: filtered.map(function (f) { return f.fileName; }) }));
                                                resolve();
                                            });
                                        }); }))];
                                case 1:
                                    _a.sent();
                                    return [2, __spreadArrays(dirs, files)];
                            }
                        });
                    });
                }
                var results;
                var _this = this;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4, new Promise(function (resolve, reject) {
                                fs_1.default.readdir(path_1.default.join(__dirname, '../results'), function (err, files) { return __awaiter(_this, void 0, void 0, function () {
                                    var fileInfo;
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0:
                                                if (err)
                                                    return [2, reject(err)];
                                                return [4, Promise.all(files.map(function (f) { return new Promise(function (resolve, reject) {
                                                        fs_1.default.stat(path_1.default.join(__dirname, '../results', f), function (err, stats) {
                                                            if (err)
                                                                return reject(err);
                                                            resolve({
                                                                fileName: f,
                                                                isDirectory: stats.isDirectory(),
                                                                subFiles: null,
                                                            });
                                                        });
                                                    }); }))];
                                            case 1:
                                                fileInfo = _a.sent();
                                                resolve(fileInfo);
                                                return [2];
                                        }
                                    });
                                }); });
                            })];
                        case 1:
                            results = _a.sent();
                            return [4, filterResults(results)];
                        case 2: return [2, _a.sent()];
                    }
                });
            });
        };
        LoaderController.selectLoader = function () {
            var _this = this;
            return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                var loaders, loaderNames;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4, this.getLoaders()];
                        case 1:
                            loaders = _a.sent();
                            loaderNames = loaders.map(function (l) { return path_1.default.parse(l).name; });
                            terminal_kit_1.terminal.clear();
                            terminal_kit_1.terminal.cyan('Choose a loader to use:\n');
                            terminal_kit_1.terminal.gridMenu(__spreadArrays(['[Exit]'], loaderNames), function (err, response) {
                                if (err)
                                    reject(err);
                                else if (response.selectedIndex == 0)
                                    process.exit();
                                else
                                    resolve(loaders[response.selectedIndex - 1]);
                            });
                            return [2];
                    }
                });
            }); });
        };
        LoaderController.selectResults = function (resultFilters) {
            return new Promise(function (resolve, reject) {
                var files = [];
                var fileCount = 0;
                function getFileCount() {
                    var length = files.filter(function (f) { return !f.isDirectory; }).length;
                    files.filter(function (f) { return f.isDirectory; }).forEach(function (f) { return f.subFiles != null ? length += f.subFiles.length : null; });
                    return length;
                }
                function addPrompt(addSubset) {
                    if (addSubset === void 0) { addSubset = false; }
                    return __awaiter(this, void 0, void 0, function () {
                        var results, fileKeys, availableResults;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4, LoaderController.getResults(resultFilters.setup, resultFilters.test)];
                                case 1:
                                    results = _a.sent();
                                    fileKeys = files.filter(function (f) { return f.isDirectory == addSubset; }).map(function (f) { return f.fileName; });
                                    availableResults = __spreadArrays([{ fileName: '[Go Back]', isDirectory: false, subFiles: null }], results.filter(function (r) { return r.isDirectory == addSubset && !fileKeys.includes(r.fileName); }));
                                    terminal_kit_1.terminal.clear();
                                    terminal_kit_1.terminal.cyan("Please choose a " + (addSubset ? 'subset' : 'file') + ":\n");
                                    terminal_kit_1.terminal.gridMenu(availableResults.map(function (r) { return path_1.default.parse(r.fileName).name; }), function (err, response) {
                                        if (err)
                                            throw err;
                                        if (response.selectedIndex > 0)
                                            files.push(availableResults[response.selectedIndex]);
                                        mainMenu();
                                    });
                                    return [2];
                            }
                        });
                    });
                }
                function addAll() {
                    return __awaiter(this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4, LoaderController.getResults(resultFilters.setup, resultFilters.test)];
                                case 1:
                                    files = _a.sent();
                                    mainMenu();
                                    return [2];
                            }
                        });
                    });
                }
                function removePrompt() {
                    terminal_kit_1.terminal.clear();
                    terminal_kit_1.terminal.cyan("Please choose an item to remove:\n");
                    terminal_kit_1.terminal.gridMenu(__spreadArrays(['[Go Back]'], files.map(function (r) { return path_1.default.parse(r.fileName).name; })), function (err, response) {
                        if (err)
                            throw err;
                        if (response.selectedIndex > 0)
                            files.splice(response.selectedIndex - 1, 1);
                        mainMenu();
                    });
                }
                function removeAll() {
                    files = [];
                    mainMenu();
                }
                function exitPrompt() {
                    terminal_kit_1.terminal.clear();
                    terminal_kit_1.terminal.cyan('Are you sure you want to exit?\n');
                    terminal_kit_1.terminal.singleColumnMenu(['Yes ', 'No '], function (err, response) {
                        if (err)
                            throw err;
                        if (response.selectedIndex == 0)
                            process.exit(0);
                        else
                            mainMenu();
                    });
                }
                function continuePrompt() {
                    terminal_kit_1.terminal.clear();
                    if (fileCount < resultFilters.minCount) {
                        terminal_kit_1.terminal.red(fileCount + " item" + (fileCount != 1 ? 's' : '') + " have been selected, but at least " + resultFilters.minCount + " are needed.\n");
                    }
                    else if (fileCount > resultFilters.maxCount && resultFilters.maxCount != 0) {
                        terminal_kit_1.terminal.red(fileCount + " item" + (fileCount != 1 ? 's' : '') + " have been selected, but a maximum of " + resultFilters.maxCount + " is allowed.\n");
                    }
                    else {
                        return resolve(files);
                    }
                    terminal_kit_1.terminal.gridMenu(['Go Back'], function (err, response) {
                        if (err)
                            throw err;
                        mainMenu();
                    });
                }
                function mainMenu() {
                    return __awaiter(this, void 0, void 0, function () {
                        function mainSelect() {
                            return new Promise(function (resolve, reject) {
                                terminal_kit_1.terminal.clear();
                                if (files.length == 0) {
                                    terminal_kit_1.terminal.cyan('Please select the result files to use for this loader.\n');
                                    terminal_kit_1.terminal.white('0 files selected\n');
                                }
                                else {
                                    terminal_kit_1.terminal.cyan(fileCount + " item" + (fileCount != 1 ? 's' : '') + " selected:\n");
                                    files.forEach(function (f) {
                                        if (f.isDirectory)
                                            terminal_kit_1.terminal.white(" - [SUBSET] " + f.fileName + "\n");
                                        else
                                            terminal_kit_1.terminal.white(" - [FILE  ] " + f.fileName + "\n");
                                    });
                                    terminal_kit_1.terminal.white('\n');
                                }
                                terminal_kit_1.terminal.green(" - A minimum of " + resultFilters.minCount + " items are required.\n");
                                if (resultFilters.maxCount != 0)
                                    terminal_kit_1.terminal.green(" - A maximum of " + resultFilters.maxCount + " items are allowed.\n");
                                if (resultFilters.setup != null)
                                    terminal_kit_1.terminal.green(" - Results must have \"" + resultFilters.setup + "\" as setup.\n");
                                if (resultFilters.test != null)
                                    terminal_kit_1.terminal.green(" - Results must have \"" + resultFilters.test + "\" as test.\n");
                                terminal_kit_1.terminal.gridMenu([
                                    'Continue',
                                    'Add File',
                                    'Add Subset',
                                    'Add All',
                                    'Remove Item',
                                    'Remove All',
                                    'Exit',
                                ], function (err, response) {
                                    if (err)
                                        reject(err);
                                    else
                                        resolve(response.selectedText);
                                });
                            });
                        }
                        var action;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    fileCount = getFileCount();
                                    return [4, mainSelect()];
                                case 1:
                                    action = _a.sent();
                                    switch (action) {
                                        case 'Continue': {
                                            continuePrompt();
                                            break;
                                        }
                                        case 'Add File': {
                                            addPrompt(false);
                                            break;
                                        }
                                        case 'Add Subset': {
                                            addPrompt(true);
                                            break;
                                        }
                                        case 'Add All': {
                                            addAll();
                                            break;
                                        }
                                        case 'Remove Item': {
                                            removePrompt();
                                            break;
                                        }
                                        case 'Remove All': {
                                            removeAll();
                                            break;
                                        }
                                        case 'Exit': {
                                            exitPrompt();
                                            break;
                                        }
                                    }
                                    return [2];
                            }
                        });
                    });
                }
                mainMenu();
            });
        };
        LoaderController.parseURI = function (URI) {
            var data = {
                setup: null,
                test: null,
                subtest: null,
                variables: {},
                uri: URI,
            };
            var parts = URI.split(':');
            if (parts.length != 2)
                throw new Error("Invalid URI: " + URI + ".");
            data.setup = parts[0];
            var parts2 = parts[1].split('/');
            if (parts2.length == 2) {
                data.test = parts2[0];
                var parts3 = parts2[1].split('?');
                if (parts3.length == 2) {
                    data.subtest = parts3[0];
                    var parts4 = parts3[1].split('&');
                    parts4.forEach(function (item) {
                        var parts5 = item.split('=');
                        if (parts5.length == 2) {
                            data.variables[parts5[0]] = parts5[1];
                        }
                        else {
                            throw new Error("Invalid variable part [ " + item + " ] in URI: " + URI + ".");
                        }
                    });
                }
                else if (parts3.length == 1) {
                    data.subtest = parts3[0];
                }
            }
            else if (parts2.length == 1) {
                var parts3 = parts2[0].split('?');
                if (parts3.length == 2) {
                    data.test = parts3[0];
                    var parts4 = parts3[1].split('&');
                    parts4.forEach(function (item) {
                        var parts5 = item.split('=');
                        if (parts5.length == 2) {
                            data.variables[parts5[0]] = parts5[1];
                        }
                        else {
                            throw new Error("Invalid variable part [ " + item + " ] in URI: " + URI + ".");
                        }
                    });
                }
                else if (parts3.length == 1) {
                    data.test = parts3[0];
                }
            }
            return data;
        };
        LoaderController.init = function () {
            return __awaiter(this, void 0, void 0, function () {
                var loaderName, loader, files, filePaths, data, spec;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4, this.selectLoader()];
                        case 1:
                            loaderName = _a.sent();
                            loader = new (require(path_1.default.join(__dirname, './loaders', loaderName)).default);
                            return [4, this.selectResults(__assign({ minCount: 1, maxCount: 0, setup: null, test: null }, loader.resultFilters))];
                        case 2:
                            files = _a.sent();
                            filePaths = [];
                            files.forEach(function (f) {
                                if (!f.isDirectory)
                                    return filePaths.push(path_1.default.join(__dirname, '../results', f.fileName));
                                else {
                                    f.subFiles.forEach(function (s) {
                                        filePaths.push(path_1.default.join(__dirname, '../results', f.fileName, s));
                                    });
                                }
                            });
                            terminal_kit_1.terminal.clear();
                            terminal_kit_1.terminal.clear();
                            data = filePaths.map(function (f) { return require(f); });
                            if (!(loader.init != null)) return [3, 4];
                            return [4, loader.init(terminal_kit_1.terminal, data)];
                        case 3:
                            _a.sent();
                            _a.label = 4;
                        case 4: return [4, loader.process(data)];
                        case 5:
                            spec = _a.sent();
                            terminal_kit_1.terminal.cyan("Graph ready:\n");
                            terminal_kit_1.terminal.white("http://localhost:3003" + graph_1.default.getEditorPath(spec));
                            terminal_kit_1.terminal.cyan('\n');
                            terminal_kit_1.terminal.singleColumnMenu(['Next Graph ', 'Exit'], function (err, response) {
                                if (err)
                                    throw err;
                                if (response.selectedIndex == 1)
                                    process.exit(0);
                                else
                                    LoaderController.init();
                            });
                            return [2];
                    }
                });
            });
        };
        return LoaderController;
    }());
    LoaderController.init();
});
