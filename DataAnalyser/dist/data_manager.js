var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "fs", "./graph"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var fs = __importStar(require("fs"));
    var graph_1 = require("./graph");
    var DataBlock = (function () {
        function DataBlock(data) {
            this.data = data;
        }
        Object.defineProperty(DataBlock.prototype, "Data", {
            get: function () {
                return this.data;
            },
            enumerable: true,
            configurable: true
        });
        return DataBlock;
    }());
    exports.default = DataBlock;
    var DB_Matrix = (function (_super) {
        __extends(DB_Matrix, _super);
        function DB_Matrix(rawData, rowLabels, columnLabels) {
            var _this = _super.call(this, rawData) || this;
            _this.GraphType = graph_1.GraphType.Heatmap;
            _this.rowLabels = rowLabels;
            _this.columnLabels = columnLabels;
            return _this;
        }
        DB_Matrix.matrixToRawData = function (data) {
            var rawData = {};
            rawData["rows"] = data.getRows();
            rawData["columns"] = data.getColumns();
            for (var i = 0; i < data.getRows(); ++i) {
                rawData[i] = {};
                for (var j = 0; j < data.getColumns(); ++j)
                    rawData[i][j] = data.getElement(i, j);
            }
            return rawData;
        };
        DB_Matrix.prototype.dataToVega = function () {
            var json = {
                name: "table",
                values: []
            };
            for (var x = 0; x < this.data["columns"]; ++x)
                for (var y = 0; y < this.data["rows"]; ++y)
                    json.values.push({
                        x: this.columnLabels == null ? x : this.columnLabels[x],
                        y: this.rowLabels == null ? y : this.rowLabels[y],
                        value: this.data[x][y]
                    });
            return json;
        };
        return DB_Matrix;
    }(DataBlock));
    exports.DB_Matrix = DB_Matrix;
    var DB_NumberList = (function (_super) {
        __extends(DB_NumberList, _super);
        function DB_NumberList(data, keys) {
            var _this = _super.call(this, data) || this;
            _this.GraphType = graph_1.GraphType.StackedBar;
            _this.keys = keys;
            return _this;
        }
        DB_NumberList.prototype.dataToVega = function () {
            var json = {
                name: "table",
                values: [],
                transform: [
                    {
                        type: "stack",
                        groupby: ["x"],
                        sort: { field: "c" },
                        field: "y"
                    }
                ]
            };
            if (this.keys == null)
                for (var key in this.data)
                    json.values.push({
                        x: key,
                        y: this.data[key],
                        c: 0
                    });
            else
                for (var key in this.data)
                    for (var i = 0; i < this.keys.length; ++i)
                        json.values.push({
                            x: key,
                            y: this.data[key][this.keys[i]],
                            c: i
                        });
            return json;
        };
        return DB_NumberList;
    }(DataBlock));
    exports.DB_NumberList = DB_NumberList;
    var DB_NumberArray = (function (_super) {
        __extends(DB_NumberArray, _super);
        function DB_NumberArray(data, keys, area) {
            if (area === void 0) { area = false; }
            var _this = _super.call(this, data) || this;
            _this.keys = keys;
            _this.area = area;
            return _this;
        }
        DB_NumberArray.prototype.dataToVega = function () {
            var json = {
                name: "table",
                values: [],
                transform: [
                    {
                        type: "stack",
                        groupby: ["x"],
                        sort: { field: "c" },
                        field: "y"
                    }
                ]
            };
            if (this.keys == null)
                for (var key in this.data) {
                    var x = isNaN(key) ? key : Number(key);
                    json.values.push({
                        x: x,
                        y: this.data[key],
                        c: 0
                    });
                }
            else
                for (var key in this.data)
                    for (var i = 0; i < this.keys.length; ++i) {
                        if (this.data[key][this.keys[i]] == null)
                            continue;
                        var x = isNaN(key) ? key : Number(key);
                        json.values.push({
                            x: x,
                            y: this.data[key][this.keys[i]],
                            c: this.keys[i]
                        });
                    }
            json.values = json.values.sort(function (a, b) {
                if (a.x < b.x)
                    return -1;
                else if (a.x > b.x)
                    return 1;
                return 0;
            });
            return json;
        };
        Object.defineProperty(DB_NumberArray.prototype, "GraphType", {
            get: function () {
                return this.area ? graph_1.GraphType.StackedArea : graph_1.GraphType.Line;
            },
            enumerable: true,
            configurable: true
        });
        ;
        return DB_NumberArray;
    }(DataBlock));
    exports.DB_NumberArray = DB_NumberArray;
    var DataManager = (function () {
        function DataManager() {
        }
        DataManager.saveDataBlock = function (db, path) {
            var json = JSON.stringify(db.Data);
            return new Promise(function (resolve, reject) {
                fs.writeFile(path, json, function (error) {
                    if (error)
                        return reject(error);
                    resolve();
                });
            });
        };
        DataManager.loadDataBlock = function (path) {
            return new Promise(function (resolve, reject) {
                fs.readFile(path, function (error, contents) {
                    if (error)
                        return reject(error);
                    var db = new DataBlock(JSON.parse(contents.toString()));
                    resolve(db);
                });
            });
        };
        return DataManager;
    }());
    exports.DataManager = DataManager;
});
